import ElementaryFunctions.DifferentiableFunction;
import ElementaryFunctions.Ident;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestPower {
    @Test
    public void testConstPow() {
        // 2 ^ x
        DifferentiableFunction fx = DifferentiableFunction.constant(2).pow(new Ident());

        // Verify on some point
        assertEquals(Math.pow(2, 2), fx.valueAt(2), 0.1);
        assertEquals(Math.pow(2, 2)*Math.log(2), fx.diff().valueAt(2), 0.1);
        assertEquals(Math.pow(2, 2)*Math.pow(Math.log(2), 2), fx.diff().diff().valueAt(2), 0.1);
    }

    @Test
    public void testPolynomPow() {
        // (2x^2 + 2) ^ (5x)
        DifferentiableFunction fx = (DifferentiableFunction.constant(2).mul(DifferentiableFunction.xToNum(2))
                .add(DifferentiableFunction.constant(2)))
                .pow(DifferentiableFunction.constant(5).mul(new Ident()));

        // Verify on some point
        assertEquals(Math.pow(4, 5), fx.valueAt(1), 0.1);
        assertEquals(5*(Math.log(4)+1)*Math.pow(4, 5), fx.diff().valueAt(1), 0.1);
    }
}
