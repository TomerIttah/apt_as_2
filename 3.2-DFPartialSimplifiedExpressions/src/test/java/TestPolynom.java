import ElementaryFunctions.DifferentiableFunction;
import ElementaryFunctions.Polynom;
import ElementaryFunctions.Recip;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestPolynom {
    static DifferentiableFunction p1;
    static DifferentiableFunction p2;

    @BeforeClass
    public static void beforeClass() {
        p1 = new Polynom(new double[]{7, 1});
        p2 = new Polynom(new double[]{13, 2, 2});
    }

    @Test
    public void testPolynomsAddition() {
        assertArrayEquals(new double[]{20, 3, 2} ,((Polynom) p1.add(p2)).getCoefficientArray(), 0.1);
        assertArrayEquals(new double[]{20, 3, 2} ,((Polynom) p2.add(p1)).getCoefficientArray(), 0.1);
    }

    @Test
    public void testPolynomsSubtraction() {
        assertArrayEquals(new double[]{-6, -1, -2} ,((Polynom) p1.sub(p2)).getCoefficientArray(), 0.1);
        assertArrayEquals(new double[]{6, 1, 2} ,((Polynom) p2.sub(p1)).getCoefficientArray(), 0.1);
    }

    @Test
    public void testPolynomsMultiplication() {
        assertArrayEquals(new double[]{91, 27, 16, 2, 0, 0} ,((Polynom) p1.mul(p2)).getCoefficientArray(), 0.1);
        assertArrayEquals(new double[]{91, 27, 16, 2, 0, 0} ,((Polynom) p2.mul(p1)).getCoefficientArray(), 0.1);
    }

    @Test
    public void testPolynomsDifferential() {
        assertArrayEquals(new double[]{1} ,((Polynom) p1.diff()).getCoefficientArray(), 0.1);
        assertArrayEquals(new double[]{2, 4} ,((Polynom) p2.diff()).getCoefficientArray(), 0.1);
    }

    @Test
    public void testPolynomsDivision() {
        DifferentiableFunction dividend = new Polynom(new double[]{-9, 0, -2, 1});
        DifferentiableFunction divisor = new Polynom(new double[]{-3, 1});
        assertArrayEquals(new double[]{3, 1, 1} ,((Polynom) dividend.div(divisor)).getCoefficientArray(), 0.1);
    }

    @Test
    public void testPolynomMixMul() {
        DifferentiableFunction p = new Polynom(new double[]{0, 1, 1});
        DifferentiableFunction r = new Recip();
        assertArrayEquals(new double[]{1, 1} ,((Polynom) p.mul(r)).getCoefficientArray(), 0.1);
        assertArrayEquals(new double[]{1, 1} ,((Polynom) r.mul(p)).getCoefficientArray(), 0.1);
    }

    @Test
    public void testPolynomComposition() {
        DifferentiableFunction p = new Polynom(new double[]{0, 1, 1});
        DifferentiableFunction r = new Recip();
        assertArrayEquals(new double[]{1, 1} ,((Polynom) p.mul(r)).getCoefficientArray(), 0.1);
        assertArrayEquals(new double[]{1, 1} ,((Polynom) r.mul(p)).getCoefficientArray(), 0.1);
    }
}
