import ElementaryFunctions.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestDifferential {

    @Test
    public void testArccosDiff() {
        DifferentiableFunction arccos = new Arccos();

        assertEquals(-1, arccos.diff().valueAt(0), 0.1);
        assertEquals(0, arccos.diff().diff().valueAt(0), 0.1);
    }

    @Test
    public void testArcsinDiff() {
        DifferentiableFunction arcsin = new Arcsin();

        assertEquals(1, arcsin.diff().valueAt(0), 0.1);
        assertEquals(0, arcsin.diff().diff().valueAt(0), 0.1);
    }

    @Test
    public void testArctanDiff() {
        DifferentiableFunction arctan = new Arctan();

        assertEquals((double) 1/2, arctan.diff().valueAt(1), 0.1);
        assertEquals((double) -1/2, arctan.diff().diff().valueAt(1), 0.1);
    }

    @Test
    public void testCosDiff() {
        DifferentiableFunction cos = new Cos();

        assertEquals(-1, cos.diff().valueAt(Math.PI / 2), 0.1);
        assertEquals(0, cos.diff().diff().valueAt(Math.PI / 2), 0.1);
        assertEquals(1, cos.diff().diff().diff().valueAt(Math.PI / 2), 0.1);
        assertEquals(0, cos.diff().diff().diff().diff().valueAt(Math.PI / 2), 0.1);
    }

    @Test
    public void testSinDiff() {
        DifferentiableFunction sin = new Sin();

        assertEquals(0, sin.diff().valueAt(Math.PI / 2), 0.1);
        assertEquals(-1, sin.diff().diff().valueAt(Math.PI / 2), 0.1);
        assertEquals(0, sin.diff().diff().diff().valueAt(Math.PI / 2), 0.1);
        assertEquals(1, sin.diff().diff().diff().diff().valueAt(Math.PI / 2), 0.1);
    }

    @Test
    public void testTanDiff() {
        DifferentiableFunction tan = new Tan();

        assertEquals(1/Math.pow(Math.cos(Math.PI / 2), 2), tan.diff().valueAt(Math.PI / 2), 0.1);
        assertEquals(2*(1/Math.pow(Math.cos(Math.PI / 2), 2))*Math.tan(Math.PI / 2),
                tan.diff().diff().valueAt(Math.PI / 2), 0.1);
    }

    @Test
    public void testExpDiff() {
        DifferentiableFunction exp = new Exp();

        assertEquals(Math.exp(2), exp.diff().valueAt(2), 0.1);
        assertEquals(Math.exp(2), exp.diff().diff().valueAt(2), 0.1);
    }

    @Test
    public void testIdentDiff() {
        DifferentiableFunction indent = new Ident();

        assertEquals(7, indent.valueAt(7), 0.1);
        assertEquals(1, indent.diff().valueAt(7), 0.1);
        assertEquals(0, indent.diff().diff().valueAt(7), 0.1);
    }

    @Test
    public void testLogDiff() {
        DifferentiableFunction log = new Log();

        assertEquals((double) 1 / 10, log.diff().valueAt(10), 0.1);
        assertEquals((double) -1 / 100, log.diff().diff().valueAt(10), 0.1);
    }

    @Test
    public void testRecipDiff() {
        DifferentiableFunction recip = new Recip();

        assertEquals((double) -1 / 100, recip.diff().valueAt(10), 0.1);
        assertEquals((double) 1/4, recip.diff().diff().valueAt(2), 0.1);
    }

    @Test
    public void testPolynomDiff() {
        // 3x^3 + 2x^2 +7x + 1
        DifferentiableFunction polynom = DifferentiableFunction.constant(3).mul(DifferentiableFunction.xToNum(3))
                .add(DifferentiableFunction.constant(2).mul(DifferentiableFunction.xToNum(2)))
                .add(DifferentiableFunction.constant(7).mul(new Ident()))
                .add(DifferentiableFunction.constant(1));

        assertEquals(47, polynom.valueAt(2), 0.1);
        assertEquals(51, polynom.diff().valueAt(2), 0.1);
        assertEquals(40, polynom.diff().diff().valueAt(2), 0.1);
        assertEquals(18, polynom.diff().diff().diff().valueAt(2), 0.1);
        assertEquals(0, polynom.diff().diff().diff().diff().valueAt(2), 0.1);
    }

    @Test
    public void testSqrtDiff() {
        // x^0.5
        DifferentiableFunction sqrt = DifferentiableFunction.xToNum(0.5);

        assertEquals(2, sqrt.valueAt(4), 0.1);
        assertEquals((double)1 / 4, sqrt.diff().valueAt(4), 0.1);
        assertEquals((double)-1 / 32, sqrt.diff().diff().valueAt(8), 0.1);
        assertEquals((double)3 / (8*Math.sqrt(32)), sqrt.diff().diff().diff().valueAt(2), 0.1);
    }
}
