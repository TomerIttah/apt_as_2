import ElementaryFunctions.DifferentiableFunction;
import ElementaryFunctions.Exp;
import ElementaryFunctions.Recip;
import ElementaryFunctions.Sin;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestComposition {
    @Test
    public void testRecipCompose() {
        //  1 / (x^2 + 5)
        DifferentiableFunction fx = (new Recip()).compose(DifferentiableFunction.xToNum(2).add(DifferentiableFunction.constant(5)));

        // Verify on some point
        assertEquals((double)1 / 6, fx.valueAt(1), 0.1);
        assertEquals((double)-1 / 18, fx.diff().valueAt(1), 0.1);
        assertEquals((double)-1 / 64, fx.diff().diff().valueAt(1), 0.1);
    }

    @Test
    public void testExpCompose() {
        //  e ^ (x^3 + 1)
        DifferentiableFunction fx = (new Exp()).compose(DifferentiableFunction.xToNum(3).add(DifferentiableFunction.constant(1)));

        // Verify on some point
        assertEquals(Math.exp(2), fx.valueAt(1), 0.1);
        assertEquals(3*Math.exp(2), fx.diff().valueAt(1), 0.1);
        assertEquals(3*(3*Math.exp(2)+2*Math.exp(2)), fx.diff().diff().valueAt(1), 0.1);
    }

    @Test
    public void testSinCompose() {
        //  sin(x^2 + 1)
        DifferentiableFunction fx = (new Sin()).compose(DifferentiableFunction.xToNum(2).add(DifferentiableFunction.constant(1)));

        // Verify on some point
        assertEquals(Math.sin(2), fx.valueAt(1), 0.1);
        assertEquals(Math.cos(2)*2, fx.diff().valueAt(1), 0.1);
        assertEquals(2*((-2)*Math.sin(2)+Math.cos(2)), fx.diff().diff().valueAt(1), 0.1);
    }
}
