import ElementaryFunctions.*;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class TestTrigo {

    @Test
    public void testTan() {
        DifferentiableFunction sin = new Sin();
        DifferentiableFunction cos = new Cos();
        DifferentiableFunction tan = new Tan();
        assertTrue(sin.div(cos) instanceof Tan);
        assertTrue(tan.mul(cos) instanceof Sin);
    }

    @Test
    public void testSinDiff() {
        DifferentiableFunction sin = new Sin();
        DifferentiableFunction d1 = sin.diff();
        assertTrue(d1 instanceof Cos);
    }
}
