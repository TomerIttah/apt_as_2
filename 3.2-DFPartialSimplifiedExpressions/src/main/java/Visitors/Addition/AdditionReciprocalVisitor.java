package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionReciprocalVisitor {
    DifferentiableFunction addVisit(Recip r1, Recip r2);
    DifferentiableFunction addVisit(Recip r, Arccos a);
    DifferentiableFunction addVisit(Recip r, Polynom p);
    DifferentiableFunction addVisit(Recip r, Arcsin a);
    DifferentiableFunction addVisit(Recip r, Arctan a);
    DifferentiableFunction addVisit(Recip r, Cos c);
    DifferentiableFunction addVisit(Recip r, Exp e);
    DifferentiableFunction addVisit(Recip r, Log l);
    DifferentiableFunction addVisit(Recip r, Sin s);
    DifferentiableFunction addVisit(Recip r, Tan t);
}
