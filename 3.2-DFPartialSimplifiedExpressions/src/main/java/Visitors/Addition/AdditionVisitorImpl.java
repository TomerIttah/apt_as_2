package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionVisitorImpl implements AdditionVisitor {

    @Override
    public DifferentiableFunction addVisit(Arccos a, DifferentiableFunction g) {
        return g.acceptAdd((AdditionArccosVisitor) new AdditionArccosVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, DifferentiableFunction g) {
        return g.acceptAdd((AdditionArcsinVisitor) new AdditionArcsinVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, DifferentiableFunction g) {
        return g.acceptAdd((AdditionArctanVisitor) new AdditionArctanVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, DifferentiableFunction g) {
        return g.acceptAdd((AdditionCosVisitor) new AdditionCosVisitorImpl(), c);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, DifferentiableFunction g) {
        return g.acceptAdd((AdditionExponentVisitor) new AdditionExponentVisitorImpl(), e);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, DifferentiableFunction g) {
        return g.acceptAdd((AdditionPolynomVisitor) new AdditionPolynomVisitorImpl(), p);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, DifferentiableFunction g) {
        return g.acceptAdd(new AdditionLogarithmVisitorImpl(), g);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, DifferentiableFunction g) {
        return g.acceptAdd((AdditionReciprocalVisitor) new AdditionReciprocalVisitorImpl(), r);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, DifferentiableFunction g) {
        return g.acceptAdd((AdditionSinVisitor) new AdditionSinVisitorImpl(), s);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, DifferentiableFunction g) {
        return g.acceptAdd((AdditionTanVisitor) new AdditionTanVisitorImpl(), t);
    }
}
