package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionCosVisitorImpl extends AdditionVisitorImpl implements AdditionCosVisitor {
    @Override
    public DifferentiableFunction addVisit(Cos c1, Cos c2) {
        return DifferentiableFunction.genericAdd(c1, c2);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Arctan a) {
        return DifferentiableFunction.genericAdd(c, a);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Arcsin a) {
        return DifferentiableFunction.genericAdd(c, a);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Polynom p) {
        if (p.isPolynomZero())
            return new Cos();
        return DifferentiableFunction.genericAdd(c, p);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Arccos a) {
        return DifferentiableFunction.genericAdd(c, a);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Exp e) {
        return DifferentiableFunction.genericAdd(c, e);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Log l) {
        return DifferentiableFunction.genericAdd(c, l);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Recip r) {
        return DifferentiableFunction.genericAdd(c, r);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Sin s) {
        return DifferentiableFunction.genericAdd(c, s);
    }

    @Override
    public DifferentiableFunction addVisit(Cos c, Tan t) {
        return DifferentiableFunction.genericAdd(c, t);
    }
}
