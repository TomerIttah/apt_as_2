package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionPolynomVisitorImpl extends AdditionVisitorImpl implements AdditionPolynomVisitor {

    public DifferentiableFunction addVisit(Polynom p1, Polynom p2) {
        double[] p1CoefficientArr = p1.getCoefficientArray();
        double[] p2CoefficientArr = p2.getCoefficientArray();
        double[] newCoefficientArray = new double[Math.max(p1CoefficientArr.length, p2CoefficientArr.length)];

        for (int exp = 0; exp < newCoefficientArray.length; exp++) {
            if ((exp < p1CoefficientArr.length) && (exp < p2CoefficientArr.length))
                newCoefficientArray[exp] = p1CoefficientArr[exp] + p2CoefficientArr[exp];
            else if (exp < p1CoefficientArr.length)
                newCoefficientArray[exp] = p1CoefficientArr[exp];
            else
                newCoefficientArray[exp] = p2CoefficientArr[exp];
        }

        return new Polynom(newCoefficientArray);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Arccos a) {
        return DifferentiableFunction.genericAdd(p, a);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Arcsin a) {
        return DifferentiableFunction.genericAdd(p, a);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Arctan a) {
        return DifferentiableFunction.genericAdd(p, a);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Cos c) {
        return DifferentiableFunction.genericAdd(p, c);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Exp e) {
        return DifferentiableFunction.genericAdd(p, e);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Log l) {
        return DifferentiableFunction.genericAdd(p, l);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Recip r) {
        return DifferentiableFunction.genericAdd(p, r);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Sin s) {
        return DifferentiableFunction.genericAdd(p, s);
    }

    @Override
    public DifferentiableFunction addVisit(Polynom p, Tan t) {
        return DifferentiableFunction.genericAdd(p, t);
    }
}
