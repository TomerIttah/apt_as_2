package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionExponentVisitor {
    DifferentiableFunction addVisit(Exp e1, Exp e2);
    DifferentiableFunction addVisit(Exp e, Polynom p);
    DifferentiableFunction addVisit(Exp e, Arccos a);
    DifferentiableFunction addVisit(Exp e, Arcsin a);
    DifferentiableFunction addVisit(Exp e, Arctan a);
    DifferentiableFunction addVisit(Exp e, Cos c);
    DifferentiableFunction addVisit(Exp e, Log l);
    DifferentiableFunction addVisit(Exp e, Recip r);
    DifferentiableFunction addVisit(Exp e, Sin s);
    DifferentiableFunction addVisit(Exp e, Tan t);
}
