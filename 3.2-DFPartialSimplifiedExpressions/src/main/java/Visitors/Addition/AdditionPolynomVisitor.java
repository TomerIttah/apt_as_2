package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionPolynomVisitor {
    DifferentiableFunction addVisit(Polynom p1, Polynom p2);
    DifferentiableFunction addVisit(Polynom p, Arccos a);
    DifferentiableFunction addVisit(Polynom p, Arcsin a);
    DifferentiableFunction addVisit(Polynom p, Arctan a);
    DifferentiableFunction addVisit(Polynom p, Cos c);
    DifferentiableFunction addVisit(Polynom p, Exp e);
    DifferentiableFunction addVisit(Polynom p, Log l);
    DifferentiableFunction addVisit(Polynom p, Recip r);
    DifferentiableFunction addVisit(Polynom p, Sin s);
    DifferentiableFunction addVisit(Polynom p, Tan t);
}
