package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionArcsinVisitor {
    DifferentiableFunction addVisit(Arcsin a1, Arcsin a2);
    DifferentiableFunction addVisit(Arcsin a, Polynom p);
    DifferentiableFunction addVisit(Arcsin arcsin, Arccos arccos);
    DifferentiableFunction addVisit(Arcsin arcsin, Arctan arctan);
    DifferentiableFunction addVisit(Arcsin a, Cos c);
    DifferentiableFunction addVisit(Arcsin a, Exp e);
    DifferentiableFunction addVisit(Arcsin a, Log l);
    DifferentiableFunction addVisit(Arcsin a, Recip r);
    DifferentiableFunction addVisit(Arcsin a, Sin s);
    DifferentiableFunction addVisit(Arcsin a, Tan t);
}
