package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionArccosVisitorImpl extends AdditionVisitorImpl implements AdditionArccosVisitor {
    @Override
    public DifferentiableFunction addVisit(Arccos a1, Arccos a2) {
        return DifferentiableFunction.genericAdd(a1, a2);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos a, Polynom p) {
        if (p.isPolynomZero())
            return new Arccos();
        return DifferentiableFunction.genericAdd(a, p);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos arccos, Arcsin arcsin) {
        return DifferentiableFunction.genericAdd(arccos, arcsin);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos arccos, Arctan arctan) {
        return DifferentiableFunction.genericAdd(arccos, arctan);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos a, Cos c) {
        return DifferentiableFunction.genericAdd(a, c);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos a, Exp e) {
        return DifferentiableFunction.genericAdd(a, e);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos a, Log l) {
        return DifferentiableFunction.genericAdd(a, l);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos a, Recip r) {
        return DifferentiableFunction.genericAdd(a, r);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos a, Sin s) {
        return DifferentiableFunction.genericAdd(a, s);
    }

    @Override
    public DifferentiableFunction addVisit(Arccos a, Tan t) {
        return DifferentiableFunction.genericAdd(a, t);
    }
}
