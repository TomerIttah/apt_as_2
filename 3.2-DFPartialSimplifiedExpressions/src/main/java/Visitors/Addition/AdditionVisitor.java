package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionVisitor {
    DifferentiableFunction addVisit(Arccos a, DifferentiableFunction g);
    DifferentiableFunction addVisit(Arcsin a, DifferentiableFunction g);
    DifferentiableFunction addVisit(Arctan a, DifferentiableFunction g);
    DifferentiableFunction addVisit(Cos c, DifferentiableFunction g);
    DifferentiableFunction addVisit(Exp e, DifferentiableFunction g);
    DifferentiableFunction addVisit(Polynom p, DifferentiableFunction g);
    DifferentiableFunction addVisit(Log l, DifferentiableFunction g);
    DifferentiableFunction addVisit(Recip r, DifferentiableFunction g);
    DifferentiableFunction addVisit(Sin s, DifferentiableFunction g);
    DifferentiableFunction addVisit(Tan t, DifferentiableFunction g);
}
