package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionArctanVisitor {
    DifferentiableFunction addVisit(Arctan arctan1, Arctan arctan2);
    DifferentiableFunction addVisit(Arctan arctan, Arcsin arcsin);
    DifferentiableFunction addVisit(Arctan a, Polynom p);
    DifferentiableFunction addVisit(Arctan arctan, Arccos arccos);
    DifferentiableFunction addVisit(Arctan a, Cos c);
    DifferentiableFunction addVisit(Arctan a, Exp e);
    DifferentiableFunction addVisit(Arctan a, Log l);
    DifferentiableFunction addVisit(Arctan a, Recip r);
    DifferentiableFunction addVisit(Arctan a, Sin s);
    DifferentiableFunction addVisit(Arctan a, Tan t);
}
