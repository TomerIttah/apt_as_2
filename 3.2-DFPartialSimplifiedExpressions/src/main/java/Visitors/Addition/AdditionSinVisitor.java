package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionSinVisitor {
    DifferentiableFunction addVisit(Sin s1, Sin s2);
    DifferentiableFunction addVisit(Sin s, Arccos a);
    DifferentiableFunction addVisit(Sin s, Polynom p);
    DifferentiableFunction addVisit(Sin s, Arcsin a);
    DifferentiableFunction addVisit(Sin s, Arctan a);
    DifferentiableFunction addVisit(Sin s, Cos c);
    DifferentiableFunction addVisit(Sin s, Exp e);
    DifferentiableFunction addVisit(Sin s, Log l);
    DifferentiableFunction addVisit(Sin s, Recip r);
    DifferentiableFunction addVisit(Sin s, Tan t);
}
