package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionArctanVisitorImpl extends AdditionVisitorImpl implements AdditionArctanVisitor {

    @Override
    public DifferentiableFunction addVisit(Arctan arctan1, Arctan arctan2) {
        return DifferentiableFunction.genericAdd(arctan1, arctan2);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan arctan, Arcsin arcsin) {
        return DifferentiableFunction.genericAdd(arctan, arcsin);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, Polynom p) {
        if (p.isPolynomZero())
            return new Arctan();
        return DifferentiableFunction.genericAdd(a, p);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan arctan, Arccos arccos) {
        return DifferentiableFunction.genericAdd(arctan, arccos);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, Cos c) {
        return DifferentiableFunction.genericAdd(a, c);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, Exp e) {
        return DifferentiableFunction.genericAdd(a, e);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, Log l) {
        return DifferentiableFunction.genericAdd(a, l);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, Recip r) {
        return DifferentiableFunction.genericAdd(a, r);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, Sin s) {
        return DifferentiableFunction.genericAdd(a, s);
    }

    @Override
    public DifferentiableFunction addVisit(Arctan a, Tan t) {
        return DifferentiableFunction.genericAdd(a, t);
    }
}
