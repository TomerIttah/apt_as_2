package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionSinVisitorImpl extends AdditionVisitorImpl implements AdditionSinVisitor {
    @Override
    public DifferentiableFunction addVisit(Sin s1, Sin s2) {
        return DifferentiableFunction.genericAdd(s1, s2);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Arccos a) {
        return DifferentiableFunction.genericAdd(s, a);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Polynom p) {
        if (p.isPolynomZero())
            return new Sin();
        return DifferentiableFunction.genericAdd(s, p);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Arcsin a) {
        return DifferentiableFunction.genericAdd(s, a);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Arctan a) {
        return DifferentiableFunction.genericAdd(s, a);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Cos c) {
        return DifferentiableFunction.genericAdd(s, c);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Exp e) {
        return DifferentiableFunction.genericAdd(s, e);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Log l) {
        return DifferentiableFunction.genericAdd(s, l);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Recip r) {
        return DifferentiableFunction.genericAdd(s, r);
    }

    @Override
    public DifferentiableFunction addVisit(Sin s, Tan t) {
        return DifferentiableFunction.genericAdd(s, t);
    }
}
