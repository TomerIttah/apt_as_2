package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionExponentVisitorImpl extends AdditionVisitorImpl implements AdditionExponentVisitor {
    @Override
    public DifferentiableFunction addVisit(Exp e1, Exp e2) {
        return DifferentiableFunction.genericAdd(e1, e2);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Polynom p) {
        if (p.isPolynomZero())
            return new Exp();
        return DifferentiableFunction.genericAdd(e, p);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Arccos a) {
        return DifferentiableFunction.genericAdd(e, a);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Arcsin a) {
        return DifferentiableFunction.genericAdd(e, a);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Arctan a) {
        return DifferentiableFunction.genericAdd(e, a);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Cos c) {
        return DifferentiableFunction.genericAdd(e, c);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Log l) {
        return DifferentiableFunction.genericAdd(e, l);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Recip r) {
        return DifferentiableFunction.genericAdd(e, r);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Sin s) {
        return DifferentiableFunction.genericAdd(e, s);
    }

    @Override
    public DifferentiableFunction addVisit(Exp e, Tan t) {
        return DifferentiableFunction.genericAdd(e, t);
    }
}
