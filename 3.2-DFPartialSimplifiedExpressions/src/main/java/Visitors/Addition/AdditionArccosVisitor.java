package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionArccosVisitor {
    DifferentiableFunction addVisit(Arccos a1, Arccos a2);
    DifferentiableFunction addVisit(Arccos a, Polynom p);
    DifferentiableFunction addVisit(Arccos arccos, Arcsin arcsin);
    DifferentiableFunction addVisit(Arccos arccos, Arctan arctan);
    DifferentiableFunction addVisit(Arccos a, Cos c);
    DifferentiableFunction addVisit(Arccos a, Exp e);
    DifferentiableFunction addVisit(Arccos a, Log l);
    DifferentiableFunction addVisit(Arccos a, Recip r);
    DifferentiableFunction addVisit(Arccos a, Sin s);
    DifferentiableFunction addVisit(Arccos a, Tan t);
}
