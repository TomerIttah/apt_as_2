package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionArcsinVisitorImpl extends AdditionVisitorImpl implements AdditionArcsinVisitor {
    @Override
    public DifferentiableFunction addVisit(Arcsin a1, Arcsin a2) {
        return DifferentiableFunction.genericAdd(a1, a2);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, Polynom p) {
        if (p.isPolynomZero())
            return new Arcsin();
        return DifferentiableFunction.genericAdd(a, p);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin arcsin, Arccos arccos) {
        return DifferentiableFunction.genericAdd(arcsin, arccos);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin arcsin, Arctan arctan) {
        return DifferentiableFunction.genericAdd(arcsin, arctan);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, Cos c) {
        return DifferentiableFunction.genericAdd(a, c);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, Exp e) {
        return DifferentiableFunction.genericAdd(a, e);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, Log l) {
        return DifferentiableFunction.genericAdd(a, l);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, Recip r) {
        return DifferentiableFunction.genericAdd(a, r);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, Sin s) {
        return DifferentiableFunction.genericAdd(a, s);
    }

    @Override
    public DifferentiableFunction addVisit(Arcsin a, Tan t) {
        return DifferentiableFunction.genericAdd(a, t);
    }
}
