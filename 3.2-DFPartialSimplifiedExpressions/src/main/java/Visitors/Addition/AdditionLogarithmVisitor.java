package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionLogarithmVisitor {
    DifferentiableFunction addVisit(Log l1, Log l2);
    DifferentiableFunction addVisit(Log l, Arccos a);
    DifferentiableFunction addVisit(Log l, Polynom p);
    DifferentiableFunction addVisit(Log l, Arcsin a);
    DifferentiableFunction addVisit(Log l, Arctan a);
    DifferentiableFunction addVisit(Log l, Cos c);
    DifferentiableFunction addVisit(Log l, Exp e);
    DifferentiableFunction addVisit(Log l, Recip r);
    DifferentiableFunction addVisit(Log l, Sin s);
    DifferentiableFunction addVisit(Log l, Tan t);
}
