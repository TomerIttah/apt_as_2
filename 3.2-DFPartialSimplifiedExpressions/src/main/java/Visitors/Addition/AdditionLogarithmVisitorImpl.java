package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionLogarithmVisitorImpl extends AdditionVisitorImpl implements AdditionLogarithmVisitor {
    @Override
    public DifferentiableFunction addVisit(Log l1, Log l2) {
        return DifferentiableFunction.genericAdd(l1, l2);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Arccos a) {
        return DifferentiableFunction.genericAdd(l, a);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Polynom p) {
        if (p.isPolynomZero())
            return new Log();
        return DifferentiableFunction.genericAdd(l, p);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Arcsin a) {
        return DifferentiableFunction.genericAdd(l, a);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Arctan a) {
        return DifferentiableFunction.genericAdd(l, a);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Cos c) {
        return DifferentiableFunction.genericAdd(l, c);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Exp e) {
        return DifferentiableFunction.genericAdd(l, e);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Recip r) {
        return DifferentiableFunction.genericAdd(l, r);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Sin s) {
        return DifferentiableFunction.genericAdd(l, s);
    }

    @Override
    public DifferentiableFunction addVisit(Log l, Tan t) {
        return DifferentiableFunction.genericAdd(l, t);
    }
}
