package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionCosVisitor {
    DifferentiableFunction addVisit(Cos c1, Cos c2);
    DifferentiableFunction addVisit(Cos c, Arctan a);
    DifferentiableFunction addVisit(Cos c, Arcsin a);
    DifferentiableFunction addVisit(Cos c, Polynom p);
    DifferentiableFunction addVisit(Cos c, Arccos a);
    DifferentiableFunction addVisit(Cos c, Exp e);
    DifferentiableFunction addVisit(Cos c, Log l);
    DifferentiableFunction addVisit(Cos c, Recip r);
    DifferentiableFunction addVisit(Cos c, Sin s);
    DifferentiableFunction addVisit(Cos c, Tan t);
}
