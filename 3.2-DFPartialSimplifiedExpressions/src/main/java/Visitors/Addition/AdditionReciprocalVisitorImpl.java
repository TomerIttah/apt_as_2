package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionReciprocalVisitorImpl extends AdditionVisitorImpl implements AdditionReciprocalVisitor {
    @Override
    public DifferentiableFunction addVisit(Recip r1, Recip r2) {
        return DifferentiableFunction.genericAdd(r1, r2);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Arccos a) {
        return DifferentiableFunction.genericAdd(r, a);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Polynom p) {
        if (p.isPolynomZero())
            return new Recip();
        return DifferentiableFunction.genericAdd(r, p);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Arcsin a) {
        return DifferentiableFunction.genericAdd(r, a);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Arctan a) {
        return DifferentiableFunction.genericAdd(r, a);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Cos c) {
        return DifferentiableFunction.genericAdd(r, c);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Exp e) {
        return DifferentiableFunction.genericAdd(r, e);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Log l) {
        return DifferentiableFunction.genericAdd(r, l);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Sin s) {
        return DifferentiableFunction.genericAdd(r, s);
    }

    @Override
    public DifferentiableFunction addVisit(Recip r, Tan t) {
        return DifferentiableFunction.genericAdd(r, t);
    }
}
