package Visitors.Addition;

import ElementaryFunctions.*;

public class AdditionTanVisitorImpl extends AdditionVisitorImpl implements AdditionTanVisitor {
    @Override
    public DifferentiableFunction addVisit(Tan t1, Tan t2) {
        return DifferentiableFunction.genericAdd(t1, t2);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Arccos a) {
        return DifferentiableFunction.genericAdd(t, a);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Polynom p) {
        if (p.isPolynomZero())
            return new Tan();
        return DifferentiableFunction.genericAdd(t, p);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Arcsin a) {
        return DifferentiableFunction.genericAdd(t, a);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Arctan a) {
        return DifferentiableFunction.genericAdd(t, a);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Cos c) {
        return DifferentiableFunction.genericAdd(t, c);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Exp e) {
        return DifferentiableFunction.genericAdd(t, e);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Log l) {
        return DifferentiableFunction.genericAdd(t, l);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Recip r) {
        return DifferentiableFunction.genericAdd(t, r);
    }

    @Override
    public DifferentiableFunction addVisit(Tan t, Sin s) {
        return DifferentiableFunction.genericAdd(t, s);
    }
}
