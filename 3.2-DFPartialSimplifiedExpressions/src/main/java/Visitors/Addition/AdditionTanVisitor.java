package Visitors.Addition;

import ElementaryFunctions.*;

public interface AdditionTanVisitor {
    DifferentiableFunction addVisit(Tan t1, Tan t2);
    DifferentiableFunction addVisit(Tan t, Arccos a);
    DifferentiableFunction addVisit(Tan t, Polynom p);
    DifferentiableFunction addVisit(Tan t, Arcsin a);
    DifferentiableFunction addVisit(Tan t, Arctan a);
    DifferentiableFunction addVisit(Tan t, Cos c);
    DifferentiableFunction addVisit(Tan t, Exp e);
    DifferentiableFunction addVisit(Tan t, Log l);
    DifferentiableFunction addVisit(Tan t, Recip r);
    DifferentiableFunction addVisit(Tan t, Sin s);
}
