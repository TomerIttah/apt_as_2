package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationReciprocalVisitor {
    DifferentiableFunction mulVisit(Recip r1, Recip r2);
    DifferentiableFunction mulVisit(Recip r, Arccos a);
    DifferentiableFunction mulVisit(Recip r, Polynom p);
    DifferentiableFunction mulVisit(Recip r, Arcsin a);
    DifferentiableFunction mulVisit(Recip r, Arctan a);
    DifferentiableFunction mulVisit(Recip r, Cos c);
    DifferentiableFunction mulVisit(Recip r, Exp e);
    DifferentiableFunction mulVisit(Recip r, Log l);
    DifferentiableFunction mulVisit(Recip r, Sin s);
    DifferentiableFunction mulVisit(Recip r, Tan t);
}
