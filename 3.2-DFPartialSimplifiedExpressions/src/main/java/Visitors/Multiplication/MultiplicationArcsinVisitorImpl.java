package Visitors.Multiplication;

import ElementaryFunctions.*;

public class MultiplicationArcsinVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationArcsinVisitor {
    @Override
    public DifferentiableFunction mulVisit(Arcsin a1, Arcsin a2) {
        return DifferentiableFunction.genericMul(a1, a2);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(a, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin arcsin, Arccos arccos) {
        return DifferentiableFunction.genericMul(arcsin, arccos);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin arcsin, Arctan arctan) {
        return DifferentiableFunction.genericMul(arcsin, arctan);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, Cos c) {
        return DifferentiableFunction.genericMul(a, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, Exp e) {
        return DifferentiableFunction.genericMul(a, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, Log l) {
        return DifferentiableFunction.genericMul(a, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, Recip r) {
        return DifferentiableFunction.genericMul(a, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, Sin s) {
        return DifferentiableFunction.genericMul(a, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, Tan t) {
        return DifferentiableFunction.genericMul(a, t);
    }
}
