package Visitors.Multiplication;

import ElementaryFunctions.*;

public class MultiplicationTanVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationTanVisitor {
    @Override
    public DifferentiableFunction mulVisit(Tan t1, Tan t2) {
        return DifferentiableFunction.genericMul(t1, t2);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Arccos a) {
        return DifferentiableFunction.genericMul(t, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(t, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Arcsin a) {
        return DifferentiableFunction.genericMul(t, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Arctan a) {
        return DifferentiableFunction.genericMul(t, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Cos c) {
        return new Sin();
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Exp e) {
        return DifferentiableFunction.genericMul(t, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Log l) {
        return DifferentiableFunction.genericMul(t, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Recip r) {
        return DifferentiableFunction.genericMul(t, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, Sin s) {
        return DifferentiableFunction.genericMul(t, s);
    }
}
