package Visitors.Multiplication;

import ElementaryFunctions.*;

public class MultiplicationArctanVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationArctanVisitor {

    @Override
    public DifferentiableFunction mulVisit(Arctan arctan1, Arctan arctan2) {
        return DifferentiableFunction.genericMul(arctan1, arctan2);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan arctan, Arcsin arcsin) {
        return DifferentiableFunction.genericMul(arctan, arcsin);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(a, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan arctan, Arccos arccos) {
        return DifferentiableFunction.genericMul(arctan, arccos);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, Cos c) {
        return DifferentiableFunction.genericMul(a, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, Exp e) {
        return DifferentiableFunction.genericMul(a, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, Log l) {
        return DifferentiableFunction.genericMul(a, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, Recip r) {
        return DifferentiableFunction.genericMul(a, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, Sin s) {
        return DifferentiableFunction.genericMul(a, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, Tan t) {
        return DifferentiableFunction.genericMul(a, t);
    }
}
