package Visitors.Multiplication;

import ElementaryFunctions.*;

public class MultiplicationVisitorImpl implements MultiplicationVisitor {
    @Override
    public DifferentiableFunction mulVisit(Arccos a, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationArccosVisitor) new MultiplicationArccosVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction mulVisit(Arcsin a, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationArcsinVisitor) new MultiplicationArcsinVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction mulVisit(Arctan a, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationArctanVisitor) new MultiplicationArctanVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationCosVisitor) new MultiplicationCosVisitorImpl(), c);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationExponentVisitor) new MultiplicationExponentVisitorImpl(), e);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p1, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationPolynomVisitor) new MultiplicationPolynomVisitorImpl(), p1);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationLogarithmVisitor) new MultiplicationLogarithmVisitorImpl(), l);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationReciprocalVisitor) new MultiplicationReciprocalVisitorImpl(), r);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationSinVisitor) new MultiplicationSinVisitorImpl(), s);
    }

    @Override
    public DifferentiableFunction mulVisit(Tan t, DifferentiableFunction g) {
        return g.acceptMul((MultiplicationTanVisitor) new MultiplicationTanVisitorImpl(), t);
    }
}
