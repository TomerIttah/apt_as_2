package Visitors.Multiplication;

import ElementaryFunctions.*;

public class MultiplicationExponentVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationExponentVisitor {
    @Override
    public DifferentiableFunction mulVisit(Exp e1, Exp e2) {
        return DifferentiableFunction.genericMul(e1, e2);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(e, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Arccos a) {
        return DifferentiableFunction.genericMul(e, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Arcsin a) {
        return DifferentiableFunction.genericMul(e, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Arctan a) {
        return DifferentiableFunction.genericMul(e, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Cos c) {
        return DifferentiableFunction.genericMul(e, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Log l) {
        return DifferentiableFunction.genericMul(e, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Recip r) {
        return DifferentiableFunction.genericMul(e, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Sin s) {
        return DifferentiableFunction.genericMul(e, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Exp e, Tan t) {
        return DifferentiableFunction.genericMul(e, t);
    }
}
