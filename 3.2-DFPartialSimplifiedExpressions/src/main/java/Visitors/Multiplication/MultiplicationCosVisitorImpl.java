package Visitors.Multiplication;

import ElementaryFunctions.*;

public class MultiplicationCosVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationCosVisitor {
    @Override
    public DifferentiableFunction mulVisit(Cos c1, Cos c2) {
        return DifferentiableFunction.genericMul(c1, c2);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Arctan a) {
        return DifferentiableFunction.genericMul(c, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Arcsin a) {
        return DifferentiableFunction.genericMul(c, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(c, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Arccos a) {
        return DifferentiableFunction.genericMul(c, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Exp e) {
        return DifferentiableFunction.genericMul(c, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Log l) {
        return DifferentiableFunction.genericMul(c, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Recip r) {
        return DifferentiableFunction.genericMul(c, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Sin s) {
        return DifferentiableFunction.genericMul(c, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Cos c, Tan t) {
        return DifferentiableFunction.genericMul(c, t);
    }
}
