package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationExponentVisitor {
    DifferentiableFunction mulVisit(Exp e1, Exp e2);
    DifferentiableFunction mulVisit(Exp e, Polynom p);
    DifferentiableFunction mulVisit(Exp e, Arccos a);
    DifferentiableFunction mulVisit(Exp e, Arcsin a);
    DifferentiableFunction mulVisit(Exp e, Arctan a);
    DifferentiableFunction mulVisit(Exp e, Cos c);
    DifferentiableFunction mulVisit(Exp e, Log l);
    DifferentiableFunction mulVisit(Exp e, Recip r);
    DifferentiableFunction mulVisit(Exp e, Sin s);
    DifferentiableFunction mulVisit(Exp e, Tan t);
}
