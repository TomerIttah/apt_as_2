package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationArctanVisitor {
    DifferentiableFunction mulVisit(Arctan arctan1, Arctan arctan2);
    DifferentiableFunction mulVisit(Arctan arctan, Arcsin arcsin);
    DifferentiableFunction mulVisit(Arctan a, Polynom p);
    DifferentiableFunction mulVisit(Arctan arctan, Arccos arccos);
    DifferentiableFunction mulVisit(Arctan a, Cos c);
    DifferentiableFunction mulVisit(Arctan a, Exp e);
    DifferentiableFunction mulVisit(Arctan a, Log l);
    DifferentiableFunction mulVisit(Arctan a, Recip r);
    DifferentiableFunction mulVisit(Arctan a, Sin s);
    DifferentiableFunction mulVisit(Arctan a, Tan t);
}
