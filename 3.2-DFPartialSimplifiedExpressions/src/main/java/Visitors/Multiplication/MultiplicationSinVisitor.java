package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationSinVisitor {
    DifferentiableFunction mulVisit(Sin s1, Sin s2);
    DifferentiableFunction mulVisit(Sin s, Arccos a);
    DifferentiableFunction mulVisit(Sin s, Polynom p);
    DifferentiableFunction mulVisit(Sin s, Arcsin a);
    DifferentiableFunction mulVisit(Sin s, Arctan a);
    DifferentiableFunction mulVisit(Sin s, Cos c);
    DifferentiableFunction mulVisit(Sin s, Exp e);
    DifferentiableFunction mulVisit(Sin s, Log l);
    DifferentiableFunction mulVisit(Sin s, Recip r);
    DifferentiableFunction mulVisit(Sin s, Tan t);
}
