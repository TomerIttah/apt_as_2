package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationArccosVisitor {
    DifferentiableFunction mulVisit(Arccos a1, Arccos a2);
    DifferentiableFunction mulVisit(Arccos a, Polynom p);
    DifferentiableFunction mulVisit(Arccos arccos, Arcsin arcsin);
    DifferentiableFunction mulVisit(Arccos arccos, Arctan arctan);
    DifferentiableFunction mulVisit(Arccos a, Cos c);
    DifferentiableFunction mulVisit(Arccos a, Exp e);
    DifferentiableFunction mulVisit(Arccos a, Log l);
    DifferentiableFunction mulVisit(Arccos a, Recip r);
    DifferentiableFunction mulVisit(Arccos a, Sin s);
    DifferentiableFunction mulVisit(Arccos a, Tan t);
}
