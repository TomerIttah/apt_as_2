package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationCosVisitor {
    DifferentiableFunction mulVisit(Cos c1, Cos c2);
    DifferentiableFunction mulVisit(Cos c, Arctan a);
    DifferentiableFunction mulVisit(Cos c, Arcsin a);
    DifferentiableFunction mulVisit(Cos c, Polynom p);
    DifferentiableFunction mulVisit(Cos c, Arccos a);
    DifferentiableFunction mulVisit(Cos c, Exp e);
    DifferentiableFunction mulVisit(Cos c, Log l);
    DifferentiableFunction mulVisit(Cos c, Recip r);
    DifferentiableFunction mulVisit(Cos c, Sin s);
    DifferentiableFunction mulVisit(Cos c, Tan t);
}
