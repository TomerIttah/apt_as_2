package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationLogarithmVisitor {
    DifferentiableFunction mulVisit(Log l1, Log l2);
    DifferentiableFunction mulVisit(Log l, Arccos a);
    DifferentiableFunction mulVisit(Log l, Polynom p);
    DifferentiableFunction mulVisit(Log l, Arcsin a);
    DifferentiableFunction mulVisit(Log l, Arctan a);
    DifferentiableFunction mulVisit(Log l, Cos c);
    DifferentiableFunction mulVisit(Log l, Exp e);
    DifferentiableFunction mulVisit(Log l, Recip r);
    DifferentiableFunction mulVisit(Log l, Sin s);
    DifferentiableFunction mulVisit(Log l, Tan t);
}
