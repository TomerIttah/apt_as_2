package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationVisitor {
    DifferentiableFunction mulVisit(Arccos a, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Arcsin a, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Arctan a, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Cos c, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Exp e, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Polynom p, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Log l, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Recip r, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Sin s, DifferentiableFunction g);
    DifferentiableFunction mulVisit(Tan t, DifferentiableFunction g);
}
