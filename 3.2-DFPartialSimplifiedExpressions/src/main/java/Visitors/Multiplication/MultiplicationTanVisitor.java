package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationTanVisitor {
    DifferentiableFunction mulVisit(Tan t1, Tan t2);
    DifferentiableFunction mulVisit(Tan t, Arccos a);
    DifferentiableFunction mulVisit(Tan t, Polynom p);
    DifferentiableFunction mulVisit(Tan t, Arcsin a);
    DifferentiableFunction mulVisit(Tan t, Arctan a);
    DifferentiableFunction mulVisit(Tan t, Cos c);
    DifferentiableFunction mulVisit(Tan t, Exp e);
    DifferentiableFunction mulVisit(Tan t, Log l);
    DifferentiableFunction mulVisit(Tan t, Recip r);
    DifferentiableFunction mulVisit(Tan t, Sin s);
}
