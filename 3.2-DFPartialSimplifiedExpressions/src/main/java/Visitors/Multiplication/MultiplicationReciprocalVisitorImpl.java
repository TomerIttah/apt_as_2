package Visitors.Multiplication;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class MultiplicationReciprocalVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationReciprocalVisitor {
    @Override
    public DifferentiableFunction mulVisit(Recip r1, Recip r2) {
        return DifferentiableFunction.genericMul(r1, r2);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Arccos a) {
        return DifferentiableFunction.genericMul(r, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return p.div(new Polynom(new double[]{0, 1}));
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Arcsin a) {
        return DifferentiableFunction.genericMul(r, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Arctan a) {
        return DifferentiableFunction.genericMul(r, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Cos c) {
        return DifferentiableFunction.genericMul(r, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Exp e) {
        return DifferentiableFunction.genericMul(r, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Log l) {
        return DifferentiableFunction.genericMul(r, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Sin s) {
        return DifferentiableFunction.genericMul(r, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Recip r, Tan t) {
        return DifferentiableFunction.genericMul(r, t);
    }
}
