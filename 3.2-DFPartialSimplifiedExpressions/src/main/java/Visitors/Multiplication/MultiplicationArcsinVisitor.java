package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationArcsinVisitor {
    DifferentiableFunction mulVisit(Arcsin a1, Arcsin a2);
    DifferentiableFunction mulVisit(Arcsin a, Polynom p);
    DifferentiableFunction mulVisit(Arcsin arcsin, Arccos arccos);
    DifferentiableFunction mulVisit(Arcsin arcsin, Arctan arctan);
    DifferentiableFunction mulVisit(Arcsin a, Cos c);
    DifferentiableFunction mulVisit(Arcsin a, Exp e);
    DifferentiableFunction mulVisit(Arcsin a, Log l);
    DifferentiableFunction mulVisit(Arcsin a, Recip r);
    DifferentiableFunction mulVisit(Arcsin a, Sin s);
    DifferentiableFunction mulVisit(Arcsin a, Tan t);
}
