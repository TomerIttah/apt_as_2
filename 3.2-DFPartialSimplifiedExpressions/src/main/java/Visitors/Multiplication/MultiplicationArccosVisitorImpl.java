package Visitors.Multiplication;

import ElementaryFunctions.*;

public class MultiplicationArccosVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationArccosVisitor {
    @Override
    public DifferentiableFunction mulVisit(Arccos a1, Arccos a2) {
        return DifferentiableFunction.genericMul(a1, a2);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos a, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(a, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos arccos, Arcsin arcsin) {
        return DifferentiableFunction.genericMul(arccos, arcsin);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos arccos, Arctan arctan) {
        return DifferentiableFunction.genericMul(arccos, arctan);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos a, Cos c) {
        return DifferentiableFunction.genericMul(a, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos a, Exp e) {
        return DifferentiableFunction.genericMul(a, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos a, Log l) {
        return DifferentiableFunction.genericMul(a, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos a, Recip r) {
        return DifferentiableFunction.genericMul(a, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos a, Sin s) {
        return DifferentiableFunction.genericMul(a, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Arccos a, Tan t) {
        return DifferentiableFunction.genericMul(a, t);
    }
}
