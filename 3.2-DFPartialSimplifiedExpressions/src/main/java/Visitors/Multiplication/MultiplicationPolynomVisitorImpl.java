package Visitors.Multiplication;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class MultiplicationPolynomVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationPolynomVisitor {

    public DifferentiableFunction mulVisit(Polynom p1, Polynom p2) {
        double[] p1CoefficientArr = p1.getCoefficientArray();
        double[] p2CoefficientArr = p2.getCoefficientArray();
        double[] newCoefficientArray = new double[p1CoefficientArr.length * p2CoefficientArr.length];

        for (int exp1 = 0; exp1 < p1CoefficientArr.length; exp1++) {
            for (int exp2 = 0; exp2 < p2CoefficientArr.length; exp2++) {
                newCoefficientArray[exp1 + exp2] += p1CoefficientArr[exp1] * p2CoefficientArr[exp2];
            }
        }

        return new Polynom(newCoefficientArray);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Arccos a) {
        return DifferentiableFunction.genericMul(p, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Arcsin a) {
        return DifferentiableFunction.genericMul(p, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Arctan a) {
        return DifferentiableFunction.genericMul(p, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Cos c) {
        return DifferentiableFunction.genericMul(p, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Exp e) {
        return DifferentiableFunction.genericMul(p, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Log l) {
        return DifferentiableFunction.genericMul(p, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Recip r) {
        return p.div(new Polynom(new double[]{0, 1}));
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Sin s) {
        return DifferentiableFunction.genericMul(p, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Polynom p, Tan t) {
        return DifferentiableFunction.genericMul(p, t);
    }
}
