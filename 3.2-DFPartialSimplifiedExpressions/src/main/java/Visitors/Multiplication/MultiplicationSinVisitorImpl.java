package Visitors.Multiplication;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class MultiplicationSinVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationSinVisitor {
    @Override
    public DifferentiableFunction mulVisit(Sin s1, Sin s2) {
        return DifferentiableFunction.genericMul(s1, s2);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Arccos a) {
        return DifferentiableFunction.genericMul(s, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(s, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Arcsin a) {
        return DifferentiableFunction.genericMul(s, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Arctan a) {
        return DifferentiableFunction.genericMul(s, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Cos c) {
        return DifferentiableFunction.genericMul(s, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Exp e) {
        return DifferentiableFunction.genericMul(s, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Log l) {
        return DifferentiableFunction.genericMul(s, l);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Recip r) {
        return DifferentiableFunction.genericMul(s, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Sin s, Tan t) {
        return DifferentiableFunction.genericMul(s, t);
    }
}
