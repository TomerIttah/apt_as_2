package Visitors.Multiplication;

import ElementaryFunctions.*;

public interface MultiplicationPolynomVisitor {
    DifferentiableFunction mulVisit(Polynom p1, Polynom p2);
    DifferentiableFunction mulVisit(Polynom p, Arccos a);
    DifferentiableFunction mulVisit(Polynom p, Arcsin a);
    DifferentiableFunction mulVisit(Polynom p, Arctan a);
    DifferentiableFunction mulVisit(Polynom p, Cos c);
    DifferentiableFunction mulVisit(Polynom p, Exp e);
    DifferentiableFunction mulVisit(Polynom p, Log l);
    DifferentiableFunction mulVisit(Polynom p, Recip r);
    DifferentiableFunction mulVisit(Polynom p, Sin s);
    DifferentiableFunction mulVisit(Polynom p, Tan t);
}
