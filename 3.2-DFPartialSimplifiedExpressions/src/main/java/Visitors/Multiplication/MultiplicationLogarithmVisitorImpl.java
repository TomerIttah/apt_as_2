package Visitors.Multiplication;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class MultiplicationLogarithmVisitorImpl extends MultiplicationVisitorImpl implements MultiplicationLogarithmVisitor {
    @Override
    public DifferentiableFunction mulVisit(Log l1, Log l2) {
        return DifferentiableFunction.genericMul(l1, l2);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Arccos a) {
        return DifferentiableFunction.genericMul(l, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Polynom p) {
        if (p.isPolynomZero())
            return DifferentiableFunction.constant(0);
        return DifferentiableFunction.genericMul(l, p);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Arcsin a) {
        return DifferentiableFunction.genericMul(l, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Arctan a) {
        return DifferentiableFunction.genericMul(l, a);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Cos c) {
        return DifferentiableFunction.genericMul(l, c);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Exp e) {
        return DifferentiableFunction.genericMul(l, e);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Recip r) {
        return DifferentiableFunction.genericMul(l, r);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Sin s) {
        return DifferentiableFunction.genericMul(l, s);
    }

    @Override
    public DifferentiableFunction mulVisit(Log l, Tan t) {
        return DifferentiableFunction.genericMul(l, t);
    }
}
