package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionCosVisitorImpl extends SubtractionVisitorImpl implements SubtractionCosVisitor {
    @Override
    public DifferentiableFunction subVisit(Cos c1, Cos c2) {
        return DifferentiableFunction.genericSub(c1, c2);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Arctan a) {
        return DifferentiableFunction.genericSub(c, a);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Arcsin a) {
        return DifferentiableFunction.genericSub(c, a);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Polynom p) {
        return DifferentiableFunction.genericSub(c, p);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Arccos a) {
        return DifferentiableFunction.genericSub(c, a);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Exp e) {
        return DifferentiableFunction.genericSub(c, e);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Log l) {
        return DifferentiableFunction.genericSub(c, l);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Recip r) {
        return DifferentiableFunction.genericSub(c, r);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Sin s) {
        return DifferentiableFunction.genericSub(c, s);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, Tan t) {
        return DifferentiableFunction.genericSub(c, t);
    }
}
