package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionVisitor {
    DifferentiableFunction subVisit(Arccos a, DifferentiableFunction g);
    DifferentiableFunction subVisit(Arcsin a, DifferentiableFunction g);
    DifferentiableFunction subVisit(Arctan a, DifferentiableFunction g);
    DifferentiableFunction subVisit(Cos c, DifferentiableFunction g);
    DifferentiableFunction subVisit(Exp e, DifferentiableFunction g);
    DifferentiableFunction subVisit(Polynom p, DifferentiableFunction g);
    DifferentiableFunction subVisit(Log l, DifferentiableFunction g);
    DifferentiableFunction subVisit(Recip r, DifferentiableFunction g);
    DifferentiableFunction subVisit(Sin s, DifferentiableFunction g);
    DifferentiableFunction subVisit(Tan t, DifferentiableFunction g);
}
