package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionVisitorImpl implements SubtractionVisitor {
    @Override
    public DifferentiableFunction subVisit(Arccos a, DifferentiableFunction g) {
        return g.acceptSub((SubtractionArccosVisitor) new SubtractionArccosVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, DifferentiableFunction g) {
        return g.acceptSub((SubtractionArcsinVisitor) new SubtractionArcsinVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, DifferentiableFunction g) {
        return g.acceptSub((SubtractionArctanVisitor) new SubtractionArctanVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction subVisit(Cos c, DifferentiableFunction g) {
        return g.acceptSub((SubtractionCosVisitor) new SubtractionCosVisitorImpl(), c);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, DifferentiableFunction g) {
        return g.acceptSub((SubtractionExponentVisitor) new SubtractionExponentVisitorImpl(), e);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, DifferentiableFunction g) {
        return g.acceptSub((SubtractionPolynomVisitor) new SubtractionPolynomVisitorImpl(), p);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, DifferentiableFunction g) {
        return g.acceptSub((SubtractionLogarithmVisitor) new SubtractionLogarithmVisitorImpl(), l);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, DifferentiableFunction g) {
        return g.acceptSub((SubtractionReciprocalVisitor) new SubtractionReciprocalVisitorImpl(), r);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, DifferentiableFunction g) {
        return g.acceptSub((SubtractionSinVisitor) new SubtractionSinVisitorImpl(), s);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, DifferentiableFunction g) {
        return g.acceptSub((SubtractionTanVisitor) new SubtractionTanVisitorImpl(), t);
    }
}
