package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionLogarithmVisitor {
    DifferentiableFunction subVisit(Log l1, Log l2);
    DifferentiableFunction subVisit(Log l, Arccos a);
    DifferentiableFunction subVisit(Log l, Polynom p);
    DifferentiableFunction subVisit(Log l, Arcsin a);
    DifferentiableFunction subVisit(Log l, Arctan a);
    DifferentiableFunction subVisit(Log l, Cos c);
    DifferentiableFunction subVisit(Log l, Exp e);
    DifferentiableFunction subVisit(Log l, Recip r);
    DifferentiableFunction subVisit(Log l, Sin s);
    DifferentiableFunction subVisit(Log l, Tan t);
}
