package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionLogarithmVisitorImpl extends SubtractionVisitorImpl implements SubtractionLogarithmVisitor {
    @Override
    public DifferentiableFunction subVisit(Log l1, Log l2) {
        return DifferentiableFunction.genericSub(l1, l2);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Arccos a) {
        return DifferentiableFunction.genericSub(l, a);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Polynom p) {
        return DifferentiableFunction.genericSub(l, p);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Arcsin a) {
        return DifferentiableFunction.genericSub(l, a);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Arctan a) {
        return DifferentiableFunction.genericSub(l, a);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Cos c) {
        return DifferentiableFunction.genericSub(l, c);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Exp e) {
        return DifferentiableFunction.genericSub(l, e);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Recip r) {
        return DifferentiableFunction.genericSub(l, r);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Sin s) {
        return DifferentiableFunction.genericSub(l, s);
    }

    @Override
    public DifferentiableFunction subVisit(Log l, Tan t) {
        return DifferentiableFunction.genericSub(l, t);
    }
}
