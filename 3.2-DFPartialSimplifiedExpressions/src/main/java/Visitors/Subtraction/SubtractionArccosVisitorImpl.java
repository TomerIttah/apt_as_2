package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionArccosVisitorImpl extends SubtractionVisitorImpl implements SubtractionArccosVisitor {
    @Override
    public DifferentiableFunction subVisit(Arccos a1, Arccos a2) {
        return DifferentiableFunction.genericSub(a1, a2);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos a, Polynom p) {
        return DifferentiableFunction.genericSub(a, p);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos arccos, Arcsin arcsin) {
        return DifferentiableFunction.genericSub(arccos, arcsin);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos arccos, Arctan arctan) {
        return DifferentiableFunction.genericSub(arccos, arctan);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos a, Cos c) {
        return DifferentiableFunction.genericSub(a, c);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos a, Exp e) {
        return DifferentiableFunction.genericSub(a, e);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos a, Log l) {
        return DifferentiableFunction.genericSub(a, l);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos a, Recip r) {
        return DifferentiableFunction.genericSub(a, r);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos a, Sin s) {
        return DifferentiableFunction.genericSub(a, s);
    }

    @Override
    public DifferentiableFunction subVisit(Arccos a, Tan t) {
        return DifferentiableFunction.genericSub(a, t);
    }
}
