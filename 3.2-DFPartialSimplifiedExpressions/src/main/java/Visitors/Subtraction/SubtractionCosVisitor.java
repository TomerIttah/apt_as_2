package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionCosVisitor {
    DifferentiableFunction subVisit(Cos c1, Cos c2);
    DifferentiableFunction subVisit(Cos c, Arctan a);
    DifferentiableFunction subVisit(Cos c, Arcsin a);
    DifferentiableFunction subVisit(Cos c, Polynom p);
    DifferentiableFunction subVisit(Cos c, Arccos a);
    DifferentiableFunction subVisit(Cos c, Exp e);
    DifferentiableFunction subVisit(Cos c, Log l);
    DifferentiableFunction subVisit(Cos c, Recip r);
    DifferentiableFunction subVisit(Cos c, Sin s);
    DifferentiableFunction subVisit(Cos c, Tan t);
}
