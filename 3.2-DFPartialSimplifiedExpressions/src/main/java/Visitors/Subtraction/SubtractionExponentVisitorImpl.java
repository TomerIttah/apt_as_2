package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionExponentVisitorImpl extends SubtractionVisitorImpl implements SubtractionExponentVisitor {
    @Override
    public DifferentiableFunction subVisit(Exp e1, Exp e2) {
        return DifferentiableFunction.genericSub(e1, e2);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Polynom p) {
        return DifferentiableFunction.genericSub(e, p);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Arccos a) {
        return DifferentiableFunction.genericSub(e, a);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Arcsin a) {
        return DifferentiableFunction.genericSub(e, a);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Arctan a) {
        return DifferentiableFunction.genericSub(e, a);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Cos c) {
        return DifferentiableFunction.genericSub(e, c);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Log l) {
        return DifferentiableFunction.genericSub(e, l);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Recip r) {
        return DifferentiableFunction.genericSub(e, r);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Sin s) {
        return DifferentiableFunction.genericSub(e, s);
    }

    @Override
    public DifferentiableFunction subVisit(Exp e, Tan t) {
        return DifferentiableFunction.genericSub(e, t);
    }
}
