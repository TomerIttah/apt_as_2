package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionReciprocalVisitorImpl extends SubtractionVisitorImpl implements SubtractionReciprocalVisitor {
    @Override
    public DifferentiableFunction subVisit(Recip r1, Recip r2) {
        return DifferentiableFunction.genericSub(r1, r2);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Arccos a) {
        return DifferentiableFunction.genericSub(r, a);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Polynom p) {
        return DifferentiableFunction.genericSub(r, p);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Arcsin a) {
        return DifferentiableFunction.genericSub(r, a);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Arctan a) {
        return DifferentiableFunction.genericSub(r, a);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Cos c) {
        return DifferentiableFunction.genericSub(r, c);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Exp e) {
        return DifferentiableFunction.genericSub(r, e);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Log l) {
        return DifferentiableFunction.genericSub(r, l);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Sin s) {
        return DifferentiableFunction.genericSub(r, s);
    }

    @Override
    public DifferentiableFunction subVisit(Recip r, Tan t) {
        return DifferentiableFunction.genericSub(r, t);
    }
}
