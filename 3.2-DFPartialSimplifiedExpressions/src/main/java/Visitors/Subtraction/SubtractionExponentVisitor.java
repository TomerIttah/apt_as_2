package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionExponentVisitor {
    DifferentiableFunction subVisit(Exp e1, Exp e2);
    DifferentiableFunction subVisit(Exp e, Polynom p);
    DifferentiableFunction subVisit(Exp e, Arccos a);
    DifferentiableFunction subVisit(Exp e, Arcsin a);
    DifferentiableFunction subVisit(Exp e, Arctan a);
    DifferentiableFunction subVisit(Exp e, Cos c);
    DifferentiableFunction subVisit(Exp e, Log l);
    DifferentiableFunction subVisit(Exp e, Recip r);
    DifferentiableFunction subVisit(Exp e, Sin s);
    DifferentiableFunction subVisit(Exp e, Tan t);
}
