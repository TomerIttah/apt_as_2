package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionTanVisitor {
    DifferentiableFunction subVisit(Tan t1, Tan t2);
    DifferentiableFunction subVisit(Tan t, Arccos a);
    DifferentiableFunction subVisit(Tan t, Polynom p);
    DifferentiableFunction subVisit(Tan t, Arcsin a);
    DifferentiableFunction subVisit(Tan t, Arctan a);
    DifferentiableFunction subVisit(Tan t, Cos c);
    DifferentiableFunction subVisit(Tan t, Exp e);
    DifferentiableFunction subVisit(Tan t, Log l);
    DifferentiableFunction subVisit(Tan t, Recip r);
    DifferentiableFunction subVisit(Tan t, Sin s);
}
