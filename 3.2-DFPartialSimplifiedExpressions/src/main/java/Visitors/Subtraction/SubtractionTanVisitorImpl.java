package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionTanVisitorImpl extends SubtractionVisitorImpl implements SubtractionTanVisitor {
    @Override
    public DifferentiableFunction subVisit(Tan t1, Tan t2) {
        return DifferentiableFunction.genericSub(t1, t2);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Arccos a) {
        return DifferentiableFunction.genericSub(t, a);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Polynom p) {
        return DifferentiableFunction.genericSub(t, p);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Arcsin a) {
        return DifferentiableFunction.genericSub(t, a);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Arctan a) {
        return DifferentiableFunction.genericSub(t, a);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Cos c) {
        return DifferentiableFunction.genericSub(t, c);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Exp e) {
        return DifferentiableFunction.genericSub(t, e);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Log l) {
        return DifferentiableFunction.genericSub(t, l);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Recip r) {
        return DifferentiableFunction.genericSub(t, r);
    }

    @Override
    public DifferentiableFunction subVisit(Tan t, Sin s) {
        return DifferentiableFunction.genericSub(t, s);
    }
}
