package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionReciprocalVisitor {
    DifferentiableFunction subVisit(Recip r1, Recip r2);
    DifferentiableFunction subVisit(Recip r, Arccos a);
    DifferentiableFunction subVisit(Recip r, Polynom p);
    DifferentiableFunction subVisit(Recip r, Arcsin a);
    DifferentiableFunction subVisit(Recip r, Arctan a);
    DifferentiableFunction subVisit(Recip r, Cos c);
    DifferentiableFunction subVisit(Recip r, Exp e);
    DifferentiableFunction subVisit(Recip r, Log l);
    DifferentiableFunction subVisit(Recip r, Sin s);
    DifferentiableFunction subVisit(Recip r, Tan t);
}
