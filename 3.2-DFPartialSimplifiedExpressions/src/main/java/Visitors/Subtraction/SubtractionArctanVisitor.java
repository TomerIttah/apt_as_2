package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionArctanVisitor {
    DifferentiableFunction subVisit(Arctan arctan1, Arctan arctan2);
    DifferentiableFunction subVisit(Arctan arctan, Arcsin arcsin);
    DifferentiableFunction subVisit(Arctan a, Polynom p);
    DifferentiableFunction subVisit(Arctan arctan, Arccos arccos);
    DifferentiableFunction subVisit(Arctan a, Cos c);
    DifferentiableFunction subVisit(Arctan a, Exp e);
    DifferentiableFunction subVisit(Arctan a, Log l);
    DifferentiableFunction subVisit(Arctan a, Recip r);
    DifferentiableFunction subVisit(Arctan a, Sin s);
    DifferentiableFunction subVisit(Arctan a, Tan t);
}
