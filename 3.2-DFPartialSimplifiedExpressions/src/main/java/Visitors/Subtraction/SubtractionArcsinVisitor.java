package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionArcsinVisitor {
    DifferentiableFunction subVisit(Arcsin a1, Arcsin a2);
    DifferentiableFunction subVisit(Arcsin a, Polynom p);
    DifferentiableFunction subVisit(Arcsin arcsin, Arccos arccos);
    DifferentiableFunction subVisit(Arcsin arcsin, Arctan arctan);
    DifferentiableFunction subVisit(Arcsin a, Cos c);
    DifferentiableFunction subVisit(Arcsin a, Exp e);
    DifferentiableFunction subVisit(Arcsin a, Log l);
    DifferentiableFunction subVisit(Arcsin a, Recip r);
    DifferentiableFunction subVisit(Arcsin a, Sin s);
    DifferentiableFunction subVisit(Arcsin a, Tan t);
}
