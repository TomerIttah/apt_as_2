package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionArctanVisitorImpl extends SubtractionVisitorImpl implements SubtractionArctanVisitor {

    @Override
    public DifferentiableFunction subVisit(Arctan arctan1, Arctan arctan2) {
        return DifferentiableFunction.genericSub(arctan1, arctan2);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan arctan, Arcsin arcsin) {
        return DifferentiableFunction.genericSub(arctan, arcsin);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, Polynom p) {
        return DifferentiableFunction.genericSub(a, p);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan arctan, Arccos arccos) {
        return DifferentiableFunction.genericSub(arctan, arccos);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, Cos c) {
        return DifferentiableFunction.genericSub(a, c);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, Exp e) {
        return DifferentiableFunction.genericSub(a, e);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, Log l) {
        return DifferentiableFunction.genericSub(a, l);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, Recip r) {
        return DifferentiableFunction.genericSub(a, r);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, Sin s) {
        return DifferentiableFunction.genericSub(a, s);
    }

    @Override
    public DifferentiableFunction subVisit(Arctan a, Tan t) {
        return DifferentiableFunction.genericSub(a, t);
    }
}
