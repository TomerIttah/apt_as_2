package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionArcsinVisitorImpl extends SubtractionVisitorImpl implements SubtractionArcsinVisitor {
    @Override
    public DifferentiableFunction subVisit(Arcsin a1, Arcsin a2) {
        return DifferentiableFunction.genericSub(a1, a2);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, Polynom p) {
        return DifferentiableFunction.genericSub(a, p);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin arcsin, Arccos arccos) {
        return DifferentiableFunction.genericSub(arcsin, arccos);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin arcsin, Arctan arctan) {
        return DifferentiableFunction.genericSub(arcsin, arctan);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, Cos c) {
        return DifferentiableFunction.genericSub(a, c);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, Exp e) {
        return DifferentiableFunction.genericSub(a, e);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, Log l) {
        return DifferentiableFunction.genericSub(a, l);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, Recip r) {
        return DifferentiableFunction.genericSub(a, r);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, Sin s) {
        return DifferentiableFunction.genericSub(a, s);
    }

    @Override
    public DifferentiableFunction subVisit(Arcsin a, Tan t) {
        return DifferentiableFunction.genericSub(a, t);
    }
}
