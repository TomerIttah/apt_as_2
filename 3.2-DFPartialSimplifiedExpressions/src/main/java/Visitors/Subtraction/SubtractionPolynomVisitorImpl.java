package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionPolynomVisitorImpl extends SubtractionVisitorImpl implements SubtractionPolynomVisitor {

    public DifferentiableFunction subVisit(Polynom p1, Polynom p2) {
        double[] p1CoefficientArr = p1.getCoefficientArray();
        double[] p2CoefficientArr = p2.getCoefficientArray();
        double[] newCoefficientArray = new double[Math.max(p1CoefficientArr.length, p2CoefficientArr.length)];

        for (int exp = 0; exp < newCoefficientArray.length; exp++) {
            if ((exp < p1CoefficientArr.length) && (exp < p2CoefficientArr.length))
                newCoefficientArray[exp] = p1CoefficientArr[exp] - p2CoefficientArr[exp];
            else if (exp < p2CoefficientArr.length)
                newCoefficientArray[exp] = -p2CoefficientArr[exp];
            else
                newCoefficientArray[exp] = p1CoefficientArr[exp];
        }

        return new Polynom(newCoefficientArray);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Arccos a) {
        return DifferentiableFunction.genericSub(p, a);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Arcsin a) {
        return DifferentiableFunction.genericSub(p, a);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Arctan a) {
        return DifferentiableFunction.genericSub(p, a);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Cos c) {
        return DifferentiableFunction.genericSub(p, c);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Exp e) {
        return DifferentiableFunction.genericAdd(p, e);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Log l) {
        return DifferentiableFunction.genericSub(p, l);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Recip r) {
        return DifferentiableFunction.genericSub(p, r);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Sin s) {
        return DifferentiableFunction.genericSub(p, s);
    }

    @Override
    public DifferentiableFunction subVisit(Polynom p, Tan t) {
        return DifferentiableFunction.genericSub(p, t);
    }
}
