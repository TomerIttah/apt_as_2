package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionPolynomVisitor {
    DifferentiableFunction subVisit(Polynom p1, Polynom p2);
    DifferentiableFunction subVisit(Polynom p, Arccos a);
    DifferentiableFunction subVisit(Polynom p, Arcsin a);
    DifferentiableFunction subVisit(Polynom p, Arctan a);
    DifferentiableFunction subVisit(Polynom p, Cos c);
    DifferentiableFunction subVisit(Polynom p, Exp e);
    DifferentiableFunction subVisit(Polynom p, Log l);
    DifferentiableFunction subVisit(Polynom p, Recip r);
    DifferentiableFunction subVisit(Polynom p, Sin s);
    DifferentiableFunction subVisit(Polynom p, Tan t);
}
