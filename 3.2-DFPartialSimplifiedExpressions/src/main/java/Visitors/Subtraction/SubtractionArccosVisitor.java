package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionArccosVisitor {
    DifferentiableFunction subVisit(Arccos a1, Arccos a2);
    DifferentiableFunction subVisit(Arccos a, Polynom p);
    DifferentiableFunction subVisit(Arccos arccos, Arcsin arcsin);
    DifferentiableFunction subVisit(Arccos arccos, Arctan arctan);
    DifferentiableFunction subVisit(Arccos a, Cos c);
    DifferentiableFunction subVisit(Arccos a, Exp e);
    DifferentiableFunction subVisit(Arccos a, Log l);
    DifferentiableFunction subVisit(Arccos a, Recip r);
    DifferentiableFunction subVisit(Arccos a, Sin s);
    DifferentiableFunction subVisit(Arccos a, Tan t);
}
