package Visitors.Subtraction;

import ElementaryFunctions.*;

public interface SubtractionSinVisitor {
    DifferentiableFunction subVisit(Sin s1, Sin s2);
    DifferentiableFunction subVisit(Sin s, Arccos a);
    DifferentiableFunction subVisit(Sin s, Polynom p);
    DifferentiableFunction subVisit(Sin s, Arcsin a);
    DifferentiableFunction subVisit(Sin s, Arctan a);
    DifferentiableFunction subVisit(Sin s, Cos c);
    DifferentiableFunction subVisit(Sin s, Exp e);
    DifferentiableFunction subVisit(Sin s, Log l);
    DifferentiableFunction subVisit(Sin s, Recip r);
    DifferentiableFunction subVisit(Sin s, Tan t);
}
