package Visitors.Subtraction;

import ElementaryFunctions.*;

public class SubtractionSinVisitorImpl extends SubtractionVisitorImpl implements SubtractionSinVisitor {
    @Override
    public DifferentiableFunction subVisit(Sin s1, Sin s2) {
        return DifferentiableFunction.genericSub(s1, s2);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Arccos a) {
        return DifferentiableFunction.genericSub(s, a);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Polynom p) {
        return DifferentiableFunction.genericSub(s, p);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Arcsin a) {
        return DifferentiableFunction.genericSub(s, a);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Arctan a) {
        return DifferentiableFunction.genericSub(s, a);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Cos c) {
        return DifferentiableFunction.genericSub(s, c);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Exp e) {
        return DifferentiableFunction.genericSub(s, e);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Log l) {
        return DifferentiableFunction.genericSub(s, l);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Recip r) {
        return DifferentiableFunction.genericSub(s, r);
    }

    @Override
    public DifferentiableFunction subVisit(Sin s, Tan t) {
        return DifferentiableFunction.genericSub(s, t);
    }
}
