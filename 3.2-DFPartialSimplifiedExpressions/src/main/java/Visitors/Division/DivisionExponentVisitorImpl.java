package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionExponentVisitorImpl extends DivisionVisitorImpl implements DivisionExponentVisitor {
    @Override
    public DifferentiableFunction divVisit(Exp e1, Exp e2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Polynom p) {
        return DifferentiableFunction.genericDiv(e, p);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Arccos a) {
        return DifferentiableFunction.genericDiv(e, a);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Arcsin a) {
        return DifferentiableFunction.genericDiv(e, a);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Arctan a) {
        return DifferentiableFunction.genericDiv(e, a);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Cos c) {
        return DifferentiableFunction.genericDiv(e, c);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Log l) {
        return DifferentiableFunction.genericDiv(e, l);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Recip r) {
        return DifferentiableFunction.genericDiv(e, r);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Sin s) {
        return DifferentiableFunction.genericDiv(e, s);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, Tan t) {
        return DifferentiableFunction.genericDiv(e, t);
    }
}
