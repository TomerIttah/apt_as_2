package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionReciprocalVisitorImpl extends DivisionVisitorImpl implements DivisionReciprocalVisitor {
    @Override
    public DifferentiableFunction divVisit(Recip r1, Recip r2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Arccos a) {
        return DifferentiableFunction.genericDiv(r, a);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Polynom p) {
        return DifferentiableFunction.genericDiv(r, p);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Arcsin a) {
        return DifferentiableFunction.genericDiv(r, a);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Arctan a) {
        return DifferentiableFunction.genericDiv(r, a);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Cos c) {
        return DifferentiableFunction.genericDiv(r, c);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Exp e) {
        return DifferentiableFunction.genericDiv(r, e);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Log l) {
        return DifferentiableFunction.genericDiv(r, l);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Sin s) {
        return DifferentiableFunction.genericDiv(r, s);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, Tan t) {
        return DifferentiableFunction.genericDiv(r, t);
    }
}
