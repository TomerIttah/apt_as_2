package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionVisitor {
    DifferentiableFunction divVisit(Arccos a, DifferentiableFunction g);
    DifferentiableFunction divVisit(Arcsin a, DifferentiableFunction g);
    DifferentiableFunction divVisit(Arctan a, DifferentiableFunction g);
    DifferentiableFunction divVisit(Cos c, DifferentiableFunction g);
    DifferentiableFunction divVisit(Exp e, DifferentiableFunction g);
    DifferentiableFunction divVisit(Polynom p, DifferentiableFunction g);
    DifferentiableFunction divVisit(Log l, DifferentiableFunction g);
    DifferentiableFunction divVisit(Recip r, DifferentiableFunction g);
    DifferentiableFunction divVisit(Sin s, DifferentiableFunction g);
    DifferentiableFunction divVisit(Tan t, DifferentiableFunction g);
}
