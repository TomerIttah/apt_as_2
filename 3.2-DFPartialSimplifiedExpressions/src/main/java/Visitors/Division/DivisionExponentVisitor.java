package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionExponentVisitor {
    DifferentiableFunction divVisit(Exp e1, Exp e2);
    DifferentiableFunction divVisit(Exp e, Polynom p);
    DifferentiableFunction divVisit(Exp e, Arccos a);
    DifferentiableFunction divVisit(Exp e, Arcsin a);
    DifferentiableFunction divVisit(Exp e, Arctan a);
    DifferentiableFunction divVisit(Exp e, Cos c);
    DifferentiableFunction divVisit(Exp e, Log l);
    DifferentiableFunction divVisit(Exp e, Recip r);
    DifferentiableFunction divVisit(Exp e, Sin s);
    DifferentiableFunction divVisit(Exp e, Tan t);
}
