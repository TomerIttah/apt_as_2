package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionCosVisitor {
    DifferentiableFunction divVisit(Cos c1, Cos c2);
    DifferentiableFunction divVisit(Cos c, Arctan a);
    DifferentiableFunction divVisit(Cos c, Arcsin a);
    DifferentiableFunction divVisit(Cos c, Polynom p);
    DifferentiableFunction divVisit(Cos c, Arccos a);
    DifferentiableFunction divVisit(Cos c, Exp e);
    DifferentiableFunction divVisit(Cos c, Log l);
    DifferentiableFunction divVisit(Cos c, Recip r);
    DifferentiableFunction divVisit(Cos c, Sin s);
    DifferentiableFunction divVisit(Cos c, Tan t);
}
