package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionCosVisitorImpl extends DivisionVisitorImpl implements DivisionCosVisitor {
    @Override
    public DifferentiableFunction divVisit(Cos c1, Cos c2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Arctan a) {
        return DifferentiableFunction.genericDiv(c, a);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Arcsin a) {
        return DifferentiableFunction.genericDiv(c, a);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Polynom p) {
        return DifferentiableFunction.genericDiv(c, p);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Arccos a) {
        return DifferentiableFunction.genericDiv(c, a);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Exp e) {
        return DifferentiableFunction.genericDiv(c, e);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Log l) {
        return DifferentiableFunction.genericDiv(c, l);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Recip r) {
        return DifferentiableFunction.genericDiv(c, r);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Sin s) {
        return DifferentiableFunction.genericDiv(c, s);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, Tan t) {
        return DifferentiableFunction.genericDiv(c, t);
    }
}
