package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionLogarithmVisitorImpl extends DivisionVisitorImpl implements DivisionLogarithmVisitor {
    @Override
    public DifferentiableFunction divVisit(Log l1, Log l2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Arccos a) {
        return DifferentiableFunction.genericDiv(l, a);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Polynom p) {
        return DifferentiableFunction.genericDiv(l, p);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Arcsin a) {
        return DifferentiableFunction.genericDiv(l, a);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Arctan a) {
        return DifferentiableFunction.genericDiv(l, a);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Cos c) {
        return DifferentiableFunction.genericDiv(l, c);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Exp e) {
        return DifferentiableFunction.genericDiv(l, e);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Recip r) {
        return DifferentiableFunction.genericDiv(l, r);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Sin s) {
        return DifferentiableFunction.genericDiv(l, s);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, Tan t) {
        return DifferentiableFunction.genericDiv(l, t);
    }
}
