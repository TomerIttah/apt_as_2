package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionPolynomVisitorImpl extends DivisionVisitorImpl implements DivisionPolynomVisitor {

    /**
     * Polynomial long division - an algorithm that implements the Euclidean division of polynomials.
     * @param p1 divisor
     * @param p2 dividend
     * @return The polynom represent the division of p1 by p2
     */
    public DifferentiableFunction divVisit(Polynom p1, Polynom p2) {
        if (p1.isPolynomZero())
            return DifferentiableFunction.constant(0);
        else if (p2.getCoefficientArray().length == 1 && p2.getCoefficientArray()[0] == 0)
            throw new ArithmeticException("Division by zero");
        else if (p1.getDegree() < p2.getDegree()) {
            return ((new Recip()).compose(p2)).mul(p1);
        }

        Polynom t;
        Polynom q = new Polynom(new double[]{0});
        Polynom r = new Polynom(p1);

        while (!r.isPolynomZero() && r.getDegree() >= p2.getDegree()) {
            t = new Polynom(new double[r.getDegree() - p2.getDegree() + 1]);
            t.getCoefficientArray()[r.getDegree() - p2.getDegree()] = r.getCoefficientArray()[r.getDegree()] / p2.getCoefficientArray()[p2.getDegree()];
            q = (Polynom) q.add(t);
            r = (Polynom) r.sub(t.mul(p2));
        }

        return q.add(r.div(p1));
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Arccos a) {
        return DifferentiableFunction.genericDiv(p, a);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Arcsin a) {
        return DifferentiableFunction.genericDiv(p, a);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Arctan a) {
        return DifferentiableFunction.genericDiv(p, a);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Cos c) {
        return DifferentiableFunction.genericDiv(p, c);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Exp e) {
        return DifferentiableFunction.genericDiv(p, e);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Log l) {
        return DifferentiableFunction.genericDiv(p, l);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Recip r) {
        return p.mul(new Polynom(new double[]{0, 1}));
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Sin s) {
        return DifferentiableFunction.genericDiv(p, s);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, Tan t) {
        return DifferentiableFunction.genericDiv(p, t);
    }
}
