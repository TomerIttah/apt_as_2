package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionArcsinVisitor {
    DifferentiableFunction divVisit(Arcsin a1, Arcsin a2);
    DifferentiableFunction divVisit(Arcsin a, Polynom p);
    DifferentiableFunction divVisit(Arcsin arcsin, Arccos arccos);
    DifferentiableFunction divVisit(Arcsin arcsin, Arctan arctan);
    DifferentiableFunction divVisit(Arcsin a, Cos c);
    DifferentiableFunction divVisit(Arcsin a, Exp e);
    DifferentiableFunction divVisit(Arcsin a, Log l);
    DifferentiableFunction divVisit(Arcsin a, Recip r);
    DifferentiableFunction divVisit(Arcsin a, Sin s);
    DifferentiableFunction divVisit(Arcsin a, Tan t);
}
