package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionLogarithmVisitor {
    DifferentiableFunction divVisit(Log l1, Log l2);
    DifferentiableFunction divVisit(Log l, Arccos a);
    DifferentiableFunction divVisit(Log l, Polynom p);
    DifferentiableFunction divVisit(Log l, Arcsin a);
    DifferentiableFunction divVisit(Log l, Arctan a);
    DifferentiableFunction divVisit(Log l, Cos c);
    DifferentiableFunction divVisit(Log l, Exp e);
    DifferentiableFunction divVisit(Log l, Recip r);
    DifferentiableFunction divVisit(Log l, Sin s);
    DifferentiableFunction divVisit(Log l, Tan t);
}
