package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionTanVisitorImpl extends DivisionVisitorImpl implements DivisionTanVisitor {
    @Override
    public DifferentiableFunction divVisit(Tan t1, Tan t2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Arccos a) {
        return DifferentiableFunction.genericDiv(t, a);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Polynom p) {
        return DifferentiableFunction.genericDiv(t, p);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Arcsin a) {
        return DifferentiableFunction.genericDiv(t, a);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Arctan a) {
        return DifferentiableFunction.genericDiv(t, a);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Cos c) {
        return DifferentiableFunction.genericDiv(t, c);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Exp e) {
        return DifferentiableFunction.genericDiv(t, e);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Log l) {
        return DifferentiableFunction.genericDiv(t, l);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Recip r) {
        return DifferentiableFunction.genericDiv(t, r);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, Sin s) {
        return DifferentiableFunction.genericDiv(t, s);
    }
}
