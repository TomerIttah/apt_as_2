package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionArccosVisitor {
    DifferentiableFunction divVisit(Arccos a1, Arccos a2);
    DifferentiableFunction divVisit(Arccos a, Polynom p);
    DifferentiableFunction divVisit(Arccos arccos, Arcsin arcsin);
    DifferentiableFunction divVisit(Arccos arccos, Arctan arctan);
    DifferentiableFunction divVisit(Arccos a, Cos c);
    DifferentiableFunction divVisit(Arccos a, Exp e);
    DifferentiableFunction divVisit(Arccos a, Log l);
    DifferentiableFunction divVisit(Arccos a, Recip r);
    DifferentiableFunction divVisit(Arccos a, Sin s);
    DifferentiableFunction divVisit(Arccos a, Tan t);
}
