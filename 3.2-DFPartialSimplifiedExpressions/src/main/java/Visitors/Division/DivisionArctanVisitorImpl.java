package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionArctanVisitorImpl extends DivisionVisitorImpl implements DivisionArctanVisitor {

    @Override
    public DifferentiableFunction divVisit(Arctan arctan1, Arctan arctan2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan arctan, Arcsin arcsin) {
        return DifferentiableFunction.genericDiv(arctan, arcsin);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, Polynom p) {
        return DifferentiableFunction.genericDiv(a, p);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan arctan, Arccos arccos) {
        return DifferentiableFunction.genericDiv(arctan, arccos);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, Cos c) {
        return DifferentiableFunction.genericDiv(a, c);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, Exp e) {
        return DifferentiableFunction.genericDiv(a, e);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, Log l) {
        return DifferentiableFunction.genericDiv(a, l);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, Recip r) {
        return DifferentiableFunction.genericDiv(a, r);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, Sin s) {
        return DifferentiableFunction.genericDiv(a, s);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, Tan t) {
        return DifferentiableFunction.genericDiv(a, t);
    }
}
