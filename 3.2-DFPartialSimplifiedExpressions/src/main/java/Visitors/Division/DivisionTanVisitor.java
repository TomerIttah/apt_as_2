package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionTanVisitor {
    DifferentiableFunction divVisit(Tan t1, Tan t2);
    DifferentiableFunction divVisit(Tan t, Arccos a);
    DifferentiableFunction divVisit(Tan t, Polynom p);
    DifferentiableFunction divVisit(Tan t, Arcsin a);
    DifferentiableFunction divVisit(Tan t, Arctan a);
    DifferentiableFunction divVisit(Tan t, Cos c);
    DifferentiableFunction divVisit(Tan t, Exp e);
    DifferentiableFunction divVisit(Tan t, Log l);
    DifferentiableFunction divVisit(Tan t, Recip r);
    DifferentiableFunction divVisit(Tan t, Sin s);
}
