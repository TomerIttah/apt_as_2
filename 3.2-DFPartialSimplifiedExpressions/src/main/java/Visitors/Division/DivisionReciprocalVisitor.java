package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionReciprocalVisitor {
    DifferentiableFunction divVisit(Recip r1, Recip r2);
    DifferentiableFunction divVisit(Recip r, Arccos a);
    DifferentiableFunction divVisit(Recip r, Polynom p);
    DifferentiableFunction divVisit(Recip r, Arcsin a);
    DifferentiableFunction divVisit(Recip r, Arctan a);
    DifferentiableFunction divVisit(Recip r, Cos c);
    DifferentiableFunction divVisit(Recip r, Exp e);
    DifferentiableFunction divVisit(Recip r, Log l);
    DifferentiableFunction divVisit(Recip r, Sin s);
    DifferentiableFunction divVisit(Recip r, Tan t);
}
