package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionSinVisitor {
    DifferentiableFunction divVisit(Sin s1, Sin s2);
    DifferentiableFunction divVisit(Sin s, Arccos a);
    DifferentiableFunction divVisit(Sin s, Polynom p);
    DifferentiableFunction divVisit(Sin s, Arcsin a);
    DifferentiableFunction divVisit(Sin s, Arctan a);
    DifferentiableFunction divVisit(Sin s, Cos c);
    DifferentiableFunction divVisit(Sin s, Exp e);
    DifferentiableFunction divVisit(Sin s, Log l);
    DifferentiableFunction divVisit(Sin s, Recip r);
    DifferentiableFunction divVisit(Sin s, Tan t);
}
