package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionPolynomVisitor {
    DifferentiableFunction divVisit(Polynom p1, Polynom p2);
    DifferentiableFunction divVisit(Polynom p, Arccos a);
    DifferentiableFunction divVisit(Polynom p, Arcsin a);
    DifferentiableFunction divVisit(Polynom p, Arctan a);
    DifferentiableFunction divVisit(Polynom p, Cos c);
    DifferentiableFunction divVisit(Polynom p, Exp e);
    DifferentiableFunction divVisit(Polynom p, Log l);
    DifferentiableFunction divVisit(Polynom p, Recip r);
    DifferentiableFunction divVisit(Polynom p, Sin s);
    DifferentiableFunction divVisit(Polynom p, Tan t);
}
