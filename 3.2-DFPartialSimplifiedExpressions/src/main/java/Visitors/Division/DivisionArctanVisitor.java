package Visitors.Division;

import ElementaryFunctions.*;

public interface DivisionArctanVisitor {
    DifferentiableFunction divVisit(Arctan arctan1, Arctan arctan2);
    DifferentiableFunction divVisit(Arctan arctan, Arcsin arcsin);
    DifferentiableFunction divVisit(Arctan a, Polynom p);
    DifferentiableFunction divVisit(Arctan arctan, Arccos arccos);
    DifferentiableFunction divVisit(Arctan a, Cos c);
    DifferentiableFunction divVisit(Arctan a, Exp e);
    DifferentiableFunction divVisit(Arctan a, Log l);
    DifferentiableFunction divVisit(Arctan a, Recip r);
    DifferentiableFunction divVisit(Arctan a, Sin s);
    DifferentiableFunction divVisit(Arctan a, Tan t);
}
