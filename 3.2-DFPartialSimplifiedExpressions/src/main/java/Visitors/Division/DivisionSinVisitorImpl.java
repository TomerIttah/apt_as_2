package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionSinVisitorImpl extends DivisionVisitorImpl implements DivisionSinVisitor {
    @Override
    public DifferentiableFunction divVisit(Sin s1, Sin s2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Arccos a) {
        return DifferentiableFunction.genericDiv(s, a);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Polynom p) {
        return DifferentiableFunction.genericDiv(s, p);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Arcsin a) {
        return DifferentiableFunction.genericDiv(s, a);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Arctan a) {
        return DifferentiableFunction.genericDiv(s, a);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Cos c) {
        return new Tan();
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Exp e) {
        return DifferentiableFunction.genericDiv(s, e);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Log l) {
        return DifferentiableFunction.genericDiv(s, l);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Recip r) {
        return DifferentiableFunction.genericDiv(s, r);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, Tan t) {
        return DifferentiableFunction.genericDiv(s, t);
    }
}
