package Visitors.Division;

import ElementaryFunctions.*;

public class DivisionVisitorImpl implements DivisionVisitor {
    @Override
    public DifferentiableFunction divVisit(Arccos a, DifferentiableFunction g) {
        return g.acceptDiv((DivisionArccosVisitor) new DivisionArccosVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, DifferentiableFunction g) {
        return g.acceptDiv((DivisionArcsinVisitor) new DivisionArcsinVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction divVisit(Arctan a, DifferentiableFunction g) {
        return g.acceptDiv((DivisionArctanVisitor) new DivisionArctanVisitorImpl(), a);
    }

    @Override
    public DifferentiableFunction divVisit(Cos c, DifferentiableFunction g) {
        return g.acceptDiv((DivisionCosVisitor) new DivisionCosVisitorImpl(), c);
    }

    @Override
    public DifferentiableFunction divVisit(Exp e, DifferentiableFunction g) {
        return g.acceptDiv((DivisionExponentVisitor) new DivisionExponentVisitorImpl(), e);
    }

    @Override
    public DifferentiableFunction divVisit(Polynom p, DifferentiableFunction g) {
        return g.acceptDiv((DivisionPolynomVisitor) new DivisionPolynomVisitorImpl(), p);
    }

    @Override
    public DifferentiableFunction divVisit(Log l, DifferentiableFunction g) {
        return g.acceptDiv(new DivisionLogarithmVisitorImpl(), g);
    }

    @Override
    public DifferentiableFunction divVisit(Recip r, DifferentiableFunction g) {
        return g.acceptDiv((DivisionReciprocalVisitor) new DivisionReciprocalVisitorImpl(), r);
    }

    @Override
    public DifferentiableFunction divVisit(Sin s, DifferentiableFunction g) {
        return g.acceptDiv((DivisionSinVisitor) new DivisionSinVisitorImpl(), s);
    }

    @Override
    public DifferentiableFunction divVisit(Tan t, DifferentiableFunction g) {
        return g.acceptDiv((DivisionTanVisitor) new DivisionTanVisitorImpl(), t);
    }
}
