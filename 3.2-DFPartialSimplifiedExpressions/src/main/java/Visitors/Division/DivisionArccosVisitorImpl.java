package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionArccosVisitorImpl extends DivisionVisitorImpl implements DivisionArccosVisitor {
    @Override
    public DifferentiableFunction divVisit(Arccos a1, Arccos a2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos a, Polynom p) {
        return DifferentiableFunction.genericDiv(a, p);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos arccos, Arcsin arcsin) {
        return DifferentiableFunction.genericDiv(arccos, arcsin);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos arccos, Arctan arctan) {
        return DifferentiableFunction.genericDiv(arccos, arctan);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos a, Cos c) {
        return DifferentiableFunction.genericDiv(a, c);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos a, Exp e) {
        return DifferentiableFunction.genericDiv(a, e);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos a, Log l) {
        return DifferentiableFunction.genericDiv(a, l);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos a, Recip r) {
        return DifferentiableFunction.genericDiv(a, r);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos a, Sin s) {
        return DifferentiableFunction.genericDiv(a, s);
    }

    @Override
    public DifferentiableFunction divVisit(Arccos a, Tan t) {
        return DifferentiableFunction.genericDiv(a, t);
    }
}
