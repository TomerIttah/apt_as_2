package Visitors.Division;

import ElementaryFunctions.*;
import Visitors.Addition.AdditionVisitorImpl;

public class DivisionArcsinVisitorImpl extends DivisionVisitorImpl implements DivisionArcsinVisitor {
    @Override
    public DifferentiableFunction divVisit(Arcsin a1, Arcsin a2) {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, Polynom p) {
        return DifferentiableFunction.genericDiv(a, p);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin arcsin, Arccos arccos) {
        return DifferentiableFunction.genericDiv(arcsin, arccos);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin arcsin, Arctan arctan) {
        return DifferentiableFunction.genericDiv(arcsin, arctan);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, Cos c) {
        return DifferentiableFunction.genericDiv(a, c);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, Exp e) {
        return DifferentiableFunction.genericDiv(a, e);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, Log l) {
        return DifferentiableFunction.genericDiv(a, l);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, Recip r) {
        return DifferentiableFunction.genericDiv(a, r);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, Sin s) {
        return DifferentiableFunction.genericDiv(a, s);
    }

    @Override
    public DifferentiableFunction divVisit(Arcsin a, Tan t) {
        return DifferentiableFunction.genericDiv(a, t);
    }
}
