package ElementaryFunctions;

import Visitors.Addition.*;
import Visitors.Division.*;
import Visitors.Multiplication.*;
import Visitors.Subtraction.*;

public class Cos extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.cos(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return (new Sin()).mul(DifferentiableFunction.constant(-1));
    }

    @Override
    public String toString() {
        return "cos(x)";
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionVisitor visitor, DifferentiableFunction g) {
        return visitor.addVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArccosVisitor visitor, Arccos g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArcsinVisitor visitor, Arcsin g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArctanVisitor visitor, Arctan g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionCosVisitor visitor, Cos g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionExponentVisitor visitor, Exp g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionLogarithmVisitor visitor, Log g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionPolynomVisitor visitor, Polynom g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionReciprocalVisitor visitor, Recip g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionSinVisitor visitor, Sin g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionTanVisitor visitor, Tan g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionVisitor visitor, DifferentiableFunction g) {
        return visitor.subVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArccosVisitor visitor, Arccos g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArcsinVisitor visitor, Arcsin g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArctanVisitor visitor, Arctan g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionCosVisitor visitor, Cos g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionExponentVisitor visitor, Exp g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionLogarithmVisitor visitor, Log g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionPolynomVisitor visitor, Polynom g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionReciprocalVisitor visitor, Recip g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionSinVisitor visitor, Sin g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionTanVisitor visitor, Tan g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationVisitor visitor, DifferentiableFunction g) {
        return visitor.mulVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArccosVisitor visitor, Arccos g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArcsinVisitor visitor, Arcsin g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArctanVisitor visitor, Arctan g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationCosVisitor visitor, Cos g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationExponentVisitor visitor, Exp g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationLogarithmVisitor visitor, Log g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationPolynomVisitor visitor, Polynom g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationReciprocalVisitor visitor, Recip g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationSinVisitor visitor, Sin g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationTanVisitor visitor, Tan g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionVisitor visitor, DifferentiableFunction g) {
        return visitor.divVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArccosVisitor visitor, Arccos g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArcsinVisitor visitor, Arcsin g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArctanVisitor visitor, Arctan g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionCosVisitor visitor, Cos g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionExponentVisitor visitor, Exp g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionLogarithmVisitor visitor, Log g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionPolynomVisitor visitor, Polynom g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionReciprocalVisitor visitor, Recip g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionSinVisitor visitor, Sin g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionTanVisitor visitor, Tan g) {
        return visitor.divVisit(g, this);
    }
}
