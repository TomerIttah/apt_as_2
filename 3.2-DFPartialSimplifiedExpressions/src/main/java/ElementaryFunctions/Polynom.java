package ElementaryFunctions;

import Visitors.Addition.*;
import Visitors.Division.*;
import Visitors.Multiplication.*;
import Visitors.Subtraction.*;

import java.util.Arrays;

// add AdditionAcceptor
public class Polynom extends DifferentiableFunction {
    protected double[] coefficientArray;

    public Polynom() {
        this.coefficientArray = new double[]{0};
    }

    public Polynom(double[] coefficientArray) {
        if (coefficientArray.length == 0)
            throw new IllegalArgumentException("ElementaryFunctions.Polynom must have at least 1 coefficient");
        this.coefficientArray = coefficientArray;
    }

    public Polynom(Polynom p1) {
        this.coefficientArray = Arrays.copyOfRange(p1.coefficientArray,0, p1.coefficientArray.length);
    }

    public double[] getCoefficientArray() {
        return coefficientArray;
    }

    public int getDegree() {
        for (int exp = coefficientArray.length - 1; exp >= 0; exp--) {
            if (coefficientArray[exp] != 0) {
                return exp;
            }
        }

        return 0;
    }

    public boolean isPolynomZero() {
        int exp = coefficientArray.length - 1;
        while(exp >= 0) {
            if (coefficientArray[exp--] != 0)
                return false;
        }

        return true;
    }

    private static double[] truncate(double[] coefficientArray) {
        if (coefficientArray[coefficientArray.length - 1] != 0)
            return coefficientArray;

        int exp = coefficientArray.length - 1;
        while(exp > 0 && coefficientArray[exp] == 0)
            exp--;
        return Arrays.copyOf(coefficientArray, exp + 1);
    }

    @Override
    public double valueAt(double x) {
        double res = 0;

        for (int exp = 0; exp < coefficientArray.length; exp++) {
            res += coefficientArray[exp] * Math.pow(x, exp);
        }

        return res;
    }

    @Override
    public DifferentiableFunction diff() {
        if (coefficientArray.length == 1) {
            return DifferentiableFunction.constant(0);
        }

        double[] newCoefficientArray = new double[coefficientArray.length - 1];

        for (int exp = 1; exp < coefficientArray.length; exp++) {
            newCoefficientArray[exp - 1] = coefficientArray[exp] * exp;
        }

        return new Polynom(newCoefficientArray);
    }



    @Override
    public String toString() {
        if (coefficientArray.length == 1)
            return String.valueOf(coefficientArray[0]);

        StringBuilder s = new StringBuilder();

        for (int exp = coefficientArray.length - 1; exp > 1; exp--) {
            if (coefficientArray[exp] != 0) {
                if (coefficientArray[exp] != 1 && exp != coefficientArray.length - 1) {
                    s.append(coefficientArray[exp] > 0 ? "+" : "").append(coefficientArray[exp]).append("x^").append(exp);
                } else if (coefficientArray[exp] != 1 && exp == coefficientArray.length - 1) {
                    s.append(coefficientArray[exp]).append("x^").append(exp);
                } else {
                    s.append("x^").append(exp);
                }
            }
        }

        if (coefficientArray[1] != 0) {
            if (coefficientArray[1] == 1)
                s.append("x");
            else
                s.append(coefficientArray[1] >= 0 && coefficientArray.length != 2 ? "+" : "")
                        .append(coefficientArray[1]).append("x");
        }

        if (coefficientArray[0] != 0)
            s.append(coefficientArray[0] >= 0 ? "+" : "").append(coefficientArray[0]);

        return s.toString();
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionVisitor visitor, DifferentiableFunction g) {
        return visitor.addVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArccosVisitor visitor, Arccos g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArcsinVisitor visitor, Arcsin g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArctanVisitor visitor, Arctan g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionCosVisitor visitor, Cos g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionExponentVisitor visitor, Exp g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionLogarithmVisitor visitor, Log g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionPolynomVisitor visitor, Polynom g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionReciprocalVisitor visitor, Recip g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionSinVisitor visitor, Sin g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionTanVisitor visitor, Tan g) {
        return visitor.addVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionVisitor visitor, DifferentiableFunction g) {
        return visitor.subVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArccosVisitor visitor, Arccos g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArcsinVisitor visitor, Arcsin g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArctanVisitor visitor, Arctan g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionCosVisitor visitor, Cos g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionExponentVisitor visitor, Exp g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionLogarithmVisitor visitor, Log g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionPolynomVisitor visitor, Polynom g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionReciprocalVisitor visitor, Recip g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionSinVisitor visitor, Sin g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionTanVisitor visitor, Tan g) {
        return visitor.subVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationVisitor visitor, DifferentiableFunction g) {
        return visitor.mulVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArccosVisitor visitor, Arccos g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArcsinVisitor visitor, Arcsin g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArctanVisitor visitor, Arctan g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationCosVisitor visitor, Cos g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationExponentVisitor visitor, Exp g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationLogarithmVisitor visitor, Log g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationPolynomVisitor visitor, Polynom g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationReciprocalVisitor visitor, Recip g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationSinVisitor visitor, Sin g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationTanVisitor visitor, Tan g) {
        return visitor.mulVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionVisitor visitor, DifferentiableFunction g) {
        return visitor.divVisit(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArccosVisitor visitor, Arccos g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArcsinVisitor visitor, Arcsin g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArctanVisitor visitor, Arctan g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionCosVisitor visitor, Cos g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionExponentVisitor visitor, Exp g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionLogarithmVisitor visitor, Log g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionPolynomVisitor visitor, Polynom g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionReciprocalVisitor visitor, Recip g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionSinVisitor visitor, Sin g) {
        return visitor.divVisit(g, this);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionTanVisitor visitor, Tan g) {
        return visitor.divVisit(g, this);
    }
}
