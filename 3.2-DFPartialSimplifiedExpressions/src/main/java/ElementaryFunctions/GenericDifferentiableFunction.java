package ElementaryFunctions;

import Visitors.Addition.*;
import Visitors.Division.*;
import Visitors.Multiplication.*;
import Visitors.Subtraction.*;

public abstract class GenericDifferentiableFunction extends DifferentiableFunction {

    @Override
    public DifferentiableFunction acceptAdd(AdditionVisitor visitor, DifferentiableFunction g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArccosVisitor visitor, Arccos g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArcsinVisitor visitor, Arcsin g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionArctanVisitor visitor, Arctan g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionCosVisitor visitor, Cos g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionExponentVisitor visitor, Exp g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionLogarithmVisitor visitor, Log g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionPolynomVisitor visitor, Polynom g) {
        if (g.isPolynomZero())
            return this;
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionReciprocalVisitor visitor, Recip g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionSinVisitor visitor, Sin g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptAdd(AdditionTanVisitor visitor, Tan g) {
        return genericAdd(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionVisitor visitor, DifferentiableFunction g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArccosVisitor visitor, Arccos g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArcsinVisitor visitor, Arcsin g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionArctanVisitor visitor, Arctan g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionCosVisitor visitor, Cos g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionExponentVisitor visitor, Exp g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionLogarithmVisitor visitor, Log g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionPolynomVisitor visitor, Polynom g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionReciprocalVisitor visitor, Recip g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionSinVisitor visitor, Sin g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptSub(SubtractionTanVisitor visitor, Tan g) {
        return genericSub(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationVisitor visitor, DifferentiableFunction g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArccosVisitor visitor, Arccos g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArcsinVisitor visitor, Arcsin g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationArctanVisitor visitor, Arctan g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationCosVisitor visitor, Cos g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationExponentVisitor visitor, Exp g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationLogarithmVisitor visitor, Log g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationPolynomVisitor visitor, Polynom g) {
        if (g.isPolynomZero())
            return constant(0);
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationReciprocalVisitor visitor, Recip g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationSinVisitor visitor, Sin g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptMul(MultiplicationTanVisitor visitor, Tan g) {
        return genericMul(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionVisitor visitor, DifferentiableFunction g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArccosVisitor visitor, Arccos g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArcsinVisitor visitor, Arcsin g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionArctanVisitor visitor, Arctan g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionCosVisitor visitor, Cos g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionExponentVisitor visitor, Exp g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionLogarithmVisitor visitor, Log g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionPolynomVisitor visitor, Polynom g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionReciprocalVisitor visitor, Recip g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionSinVisitor visitor, Sin g) {
        return genericDiv(this, g);
    }

    @Override
    public DifferentiableFunction acceptDiv(DivisionTanVisitor visitor, Tan g) {
        return genericDiv(this, g);
    }
}
