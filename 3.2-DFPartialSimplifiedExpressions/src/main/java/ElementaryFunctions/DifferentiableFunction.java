package ElementaryFunctions;

import Acceptors.AdditionAcceptor;
import Acceptors.DivisionAcceptor;
import Acceptors.MultiplicationAcceptor;
import Acceptors.SubtractionAcceptor;
import Visitors.Addition.AdditionVisitorImpl;
import Visitors.Division.DivisionVisitorImpl;
import Visitors.Multiplication.MultiplicationVisitorImpl;
import Visitors.Subtraction.SubtractionVisitorImpl;

public abstract class DifferentiableFunction implements AdditionAcceptor, SubtractionAcceptor, MultiplicationAcceptor, DivisionAcceptor {

    public static DifferentiableFunction constant(double c) {
        return new Polynom(new double[]{c});
    }

    public static DifferentiableFunction xToNum(double r) {
        if (r == 0)
            throw new IllegalArgumentException("r should be non zero");

        return (new Ident()).pow(constant(r));
    }

    public abstract double valueAt(double x);

    public abstract DifferentiableFunction diff();

    public DifferentiableFunction compose(DifferentiableFunction g) {
        DifferentiableFunction cur = this;
        return new GenericDifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return cur.valueAt(g.valueAt(x));
            }

            @Override
            public DifferentiableFunction diff() {
                // The chain rule
                return (cur.diff().compose(g)).mul(g.diff());
            }

            @Override
            public String toString() {
                return cur.toString().replaceAll("x", "(" + g.toString() + ")");
            }
        };
    }

    public static DifferentiableFunction genericAdd(DifferentiableFunction f1, DifferentiableFunction f2) {
        return new GenericDifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return f1.valueAt(x) + f2.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) + g(x))' = f'(x) + g'(x)
                return f1.diff().add(f2.diff());
            }

            @Override
            public String toString() {
                return f1 + " + " + f2;
            }
        };
    }

    public static DifferentiableFunction genericSub(DifferentiableFunction f1, DifferentiableFunction f2) {
        return new GenericDifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return f1.valueAt(x) - f2.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) - g(x))' = f'(x) - g'(x)
                return f1.diff().sub(f2.diff());
            }

            @Override
            public String toString() {
                return f1  + " - " + f2;
            }
        };
    }

    public static DifferentiableFunction genericMul(DifferentiableFunction f1, DifferentiableFunction f2) {
        return new GenericDifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return f1.valueAt(x) * f2.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) * g(x))' = f'(x)*g(x) + g'(x)*f(x)
                return (f1.mul(f2.diff())).add(f1.diff().mul(f2));
            }

            @Override
            public String toString() {
                return "((" + f1 + ")" + " * " + "(" + f2 + "))";
            }
        };
    }

    public static DifferentiableFunction genericDiv(DifferentiableFunction f1, DifferentiableFunction f2) {
        return new GenericDifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return f1.valueAt(x) / f2.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) / g(x))' = (f'(x)*g(x) - g'(x)*f(x)) / g(x)^2
                return (f1.diff().mul(f2)).add(constant(-1).mul(f2.diff().mul(f1))).div(f2.pow(constant(2)));
            }

            @Override
            public String toString() {
                return "((" + f1 + ")" + " / " + "(" + f2 + "))";
            }
        };
    }

    public DifferentiableFunction add(DifferentiableFunction g) {
        return this.acceptAdd(new AdditionVisitorImpl(), g);
    }

    public DifferentiableFunction sub(DifferentiableFunction g) {
        return this.acceptSub(new SubtractionVisitorImpl(), g);
    }

    public DifferentiableFunction mul(DifferentiableFunction g) {
        return this.acceptMul(new MultiplicationVisitorImpl(), g);
    }

    public DifferentiableFunction div(DifferentiableFunction g) {
        return this.acceptDiv(new DivisionVisitorImpl(), g);
    }

    public DifferentiableFunction pow(DifferentiableFunction g) {
        DifferentiableFunction cur = this;
        return new GenericDifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return Math.pow(cur.valueAt(x), g.valueAt(x));
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) ^ g(x))' = (e ^ (g(x)*ln(f(x))))'
                return (new Exp()).compose(g.mul((new Log()).compose(cur))).diff();
            }
        };
    }
}
