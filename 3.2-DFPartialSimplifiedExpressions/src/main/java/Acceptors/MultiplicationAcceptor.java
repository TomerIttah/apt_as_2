package Acceptors;

import ElementaryFunctions.*;
import Visitors.Multiplication.*;

public interface MultiplicationAcceptor {
    DifferentiableFunction acceptMul(MultiplicationVisitor visitor, DifferentiableFunction g);
    DifferentiableFunction acceptMul(MultiplicationArccosVisitor visitor, Arccos g);
    DifferentiableFunction acceptMul(MultiplicationArcsinVisitor visitor, Arcsin g);
    DifferentiableFunction acceptMul(MultiplicationArctanVisitor visitor, Arctan g);
    DifferentiableFunction acceptMul(MultiplicationCosVisitor visitor, Cos g);
    DifferentiableFunction acceptMul(MultiplicationExponentVisitor visitor, Exp g);
    DifferentiableFunction acceptMul(MultiplicationLogarithmVisitor visitor, Log g);
    DifferentiableFunction acceptMul(MultiplicationPolynomVisitor visitor, Polynom g);
    DifferentiableFunction acceptMul(MultiplicationReciprocalVisitor visitor, Recip g);
    DifferentiableFunction acceptMul(MultiplicationSinVisitor visitor, Sin g);
    DifferentiableFunction acceptMul(MultiplicationTanVisitor visitor, Tan g);
}
