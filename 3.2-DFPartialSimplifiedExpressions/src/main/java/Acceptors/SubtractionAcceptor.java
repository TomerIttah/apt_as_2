package Acceptors;

import ElementaryFunctions.*;
import Visitors.Subtraction.*;

public interface SubtractionAcceptor {
    DifferentiableFunction acceptSub(SubtractionVisitor visitor, DifferentiableFunction g);
    DifferentiableFunction acceptSub(SubtractionArccosVisitor visitor, Arccos g);
    DifferentiableFunction acceptSub(SubtractionArcsinVisitor visitor, Arcsin g);
    DifferentiableFunction acceptSub(SubtractionArctanVisitor visitor, Arctan g);
    DifferentiableFunction acceptSub(SubtractionCosVisitor visitor, Cos g);
    DifferentiableFunction acceptSub(SubtractionExponentVisitor visitor, Exp g);
    DifferentiableFunction acceptSub(SubtractionLogarithmVisitor visitor, Log l);
    DifferentiableFunction acceptSub(SubtractionPolynomVisitor visitor, Polynom g);
    DifferentiableFunction acceptSub(SubtractionReciprocalVisitor visitor, Recip g);
    DifferentiableFunction acceptSub(SubtractionSinVisitor visitor, Sin g);
    DifferentiableFunction acceptSub(SubtractionTanVisitor visitor, Tan g);
}
