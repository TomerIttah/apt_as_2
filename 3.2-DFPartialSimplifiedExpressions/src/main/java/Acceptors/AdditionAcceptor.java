package Acceptors;

import ElementaryFunctions.*;
import Visitors.Addition.*;

public interface AdditionAcceptor {
    DifferentiableFunction acceptAdd(AdditionVisitor visitor, DifferentiableFunction g);
    DifferentiableFunction acceptAdd(AdditionArccosVisitor visitor, Arccos g);
    DifferentiableFunction acceptAdd(AdditionArcsinVisitor visitor, Arcsin g);
    DifferentiableFunction acceptAdd(AdditionArctanVisitor visitor, Arctan g);
    DifferentiableFunction acceptAdd(AdditionCosVisitor visitor, Cos g);
    DifferentiableFunction acceptAdd(AdditionExponentVisitor visitor, Exp g);
    DifferentiableFunction acceptAdd(AdditionLogarithmVisitor visitor, Log g);
    DifferentiableFunction acceptAdd(AdditionPolynomVisitor visitor, Polynom g);
    DifferentiableFunction acceptAdd(AdditionReciprocalVisitor visitor, Recip g);
    DifferentiableFunction acceptAdd(AdditionSinVisitor visitor, Sin g);
    DifferentiableFunction acceptAdd(AdditionTanVisitor visitor, Tan g);
}
