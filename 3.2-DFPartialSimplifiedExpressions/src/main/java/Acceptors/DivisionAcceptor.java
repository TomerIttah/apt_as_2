package Acceptors;

import ElementaryFunctions.*;
import Visitors.Division.*;

public interface DivisionAcceptor {
    DifferentiableFunction acceptDiv(DivisionVisitor visitor, DifferentiableFunction g);
    DifferentiableFunction acceptDiv(DivisionArccosVisitor visitor, Arccos g);
    DifferentiableFunction acceptDiv(DivisionArcsinVisitor visitor, Arcsin g);
    DifferentiableFunction acceptDiv(DivisionArctanVisitor visitor, Arctan g);
    DifferentiableFunction acceptDiv(DivisionCosVisitor visitor, Cos g);
    DifferentiableFunction acceptDiv(DivisionExponentVisitor visitor, Exp g);
    DifferentiableFunction acceptDiv(DivisionLogarithmVisitor visitor, Log g);
    DifferentiableFunction acceptDiv(DivisionPolynomVisitor visitor, Polynom g);
    DifferentiableFunction acceptDiv(DivisionReciprocalVisitor visitor, Recip g);
    DifferentiableFunction acceptDiv(DivisionSinVisitor visitor, Sin g);
    DifferentiableFunction acceptDiv(DivisionTanVisitor visitor, Tan g);
}
