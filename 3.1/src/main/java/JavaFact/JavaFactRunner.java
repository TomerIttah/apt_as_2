package JavaFact;

import JavaFact.ChurchNumerals;

public class JavaFactRunner {

    public static int fact(int n) {
        LambdaExpression S = new S();
        LambdaExpression K = new K();

        return ChurchNumerals.churchToInteger(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(K).apply(K)).apply(S.apply(S.apply(K.apply(S)).apply(K).apply(S.apply(K.apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)))).apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(K).apply(K)))).apply(S.apply(K.apply(S)).apply(K).apply(S.apply(S.apply(K.apply(S)).apply(K))).apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(K).apply(K)).apply(K)))).apply(S.apply(S.apply(K.apply(S)).apply(K).apply(S.apply(K.apply(S)).apply(K)).apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(K).apply(K)).apply(K))).apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(K).apply(K)).apply(K.apply(S.apply(K).apply(K))))))).apply(S.apply(K.apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)))).apply(S.apply(S.apply(K.apply(S)).apply(S.apply(K.apply(K)).apply(S))).apply(K.apply(K)).apply(S.apply(K).apply(K))).apply(S.apply(S.apply(K.apply(S)).apply(K)).apply(K.apply(S.apply(K).apply(K)))).apply(S.apply(S.apply(K.apply(S)).apply(K)).apply(K.apply(S.apply(K).apply(K)))))).apply(K.apply(S.apply(K).apply(K)))
        .apply(ChurchNumerals.IntegerToChurch(n)));
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Usage: java -jar JavaFact.JavaFactRunner n");
            System.exit(1);
        }

        System.out.println(fact(Integer.parseInt(args[0])));
    }
}
