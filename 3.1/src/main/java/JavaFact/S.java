package JavaFact;

import JavaFact.LambdaExpression;

public class S implements LambdaExpression {

    @Override
    public LambdaExpression apply(LambdaExpression arg) {
        return y -> z -> arg.apply(z).apply(y.apply(z));
    }
}
