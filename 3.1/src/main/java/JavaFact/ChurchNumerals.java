package JavaFact;

public class ChurchNumerals {

    public static LambdaExpression c0() {
        return s -> z -> z;
    }

    public static LambdaExpression successor() {
        return n -> s -> z -> s.apply(n.apply(s).apply(z));
    }

    public static LambdaExpression addition() {
        return ca -> cb -> ca.apply(successor()).apply(cb);
    }

    public static LambdaExpression IntegerToChurch(int n) {
        if (n == 0)
            return c0();
        else
            return successor().apply(IntegerToChurch(n - 1));
    }

    public static int churchToInteger(LambdaExpression cn) {
        int[] box = new int[]{0};
        LambdaExpression counter = f -> {
            box[0]++;
            return f;
        };

        cn.apply(counter).apply(c0());
        return box[0];
    }
}
