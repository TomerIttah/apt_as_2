package JavaFact;

public interface LambdaExpression {
    public LambdaExpression apply(LambdaExpression arg);
}
