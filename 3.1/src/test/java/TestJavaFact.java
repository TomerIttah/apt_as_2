import JavaFact.JavaFactRunner;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestJavaFact {

    @Test
    public void test_0() {
        assertEquals(1, JavaFactRunner.fact(0));
    }

    @Test
    public void test_1() {
        assertEquals(1, JavaFactRunner.fact(1));
    }

    @Test
    public void test_2() {
        assertEquals(2, JavaFactRunner.fact(2));
    }

    @Test
    public void test_3() {
        assertEquals(6, JavaFactRunner.fact(3));
    }

    @Test
    public void test_4() {
        assertEquals(24, JavaFactRunner.fact(4));
    }

    @Test
    public void test_5() {
        assertEquals(120, JavaFactRunner.fact(5));
    }
}
