package ElementaryFunctions;

public abstract class DifferentiableFunction {

    public static DifferentiableFunction constant(double c) {
        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return c;
            }

            @Override
            public DifferentiableFunction diff() {
                return DifferentiableFunction.constant(0);
            }

            @Override
            public String toString() {
                return String.valueOf(c);
            }
        };
    }

    public static DifferentiableFunction xToNum(double r) {
        if (r == 0)
            throw new IllegalArgumentException();
        else if (r == 1)
            return new Ident();

        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return Math.pow(x, r);
            }

            @Override
            public DifferentiableFunction diff() {
                return constant(r).mul(xToNum(r-1));
            }

            @Override
            public String toString() {
                return "x^" + r;
            }
        };
    }

    public abstract double valueAt(double x);

    public abstract DifferentiableFunction diff();

    public DifferentiableFunction compose(DifferentiableFunction g) {
        DifferentiableFunction cur = this;

        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return cur.valueAt(g.valueAt(x));
            }

            @Override
            public DifferentiableFunction diff() {
                // The chain rule
                return (cur.diff().compose(g)).mul(g.diff());
            }

            @Override
            public String toString() {
                return cur.toString().replaceAll("x", "(" + g + ")");
            }
        };
    }

    public DifferentiableFunction add(DifferentiableFunction g) {
        DifferentiableFunction cur = this;

        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return cur.valueAt(x) + g.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) + g(x))' = f'(x) + g'(x)
                return cur.diff().add(g.diff());
            }

            @Override
            public String toString() {
                return cur + " + " + g;
            }
        };
    }

    public DifferentiableFunction sub(DifferentiableFunction g) {
        DifferentiableFunction cur = this;

        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return cur.valueAt(x) - g.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) - g(x))' = f'(x) - g'(x)
                return cur.diff().sub(g.diff());
            }

            @Override
            public String toString() {
                return cur  + " - " + g;
            }
        };
    }

    public DifferentiableFunction mul(DifferentiableFunction g) {
        DifferentiableFunction cur = this;

        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return cur.valueAt(x) * g.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) * g(x))' = f'(x)*g(x) + g'(x)*f(x)
                return (cur.mul(g.diff())).add(cur.diff().mul(g));
            }

            @Override
            public String toString() {
                return "((" + cur + ")" + " * " + "(" + g + "))";
            }
        };
    }

    public DifferentiableFunction div(DifferentiableFunction g) {
        DifferentiableFunction cur = this;

        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return cur.valueAt(x) / g.valueAt(x);
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) / g(x))' = (f'(x)*g(x) - g'(x)*f(x)) / g(x)^2
                return (cur.diff().mul(g).sub(g.diff().mul(cur))).div(g.pow(constant(2)));
            }

            @Override
            public String toString() {
                return "((" + cur + ")" + " / " + "(" + g + "))";
            }
        };
    }

    public DifferentiableFunction pow(DifferentiableFunction g) {
        DifferentiableFunction cur = this;

        return new DifferentiableFunction() {
            @Override
            public double valueAt(double x) {
                return Math.pow(cur.valueAt(x), g.valueAt(x));
            }

            @Override
            public DifferentiableFunction diff() {
                // (f(x) ^ g(x))' = (e ^ (g(x)*ln(f(x))))'
                return ((new Exp()).compose(g.mul((new Log()).compose(cur)))).diff();
            }

            @Override
            public String toString() {
                return "((" + cur + ")" + " ^ " + "(" + g + "))";
            }
        };
    }
}
