package ElementaryFunctions;

public class Arctan extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.atan(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return (new Recip())
                .compose(DifferentiableFunction.xToNum(2).add(DifferentiableFunction.constant(1)));
    }

    @Override
    public String toString() {
        return "arctan(x)";
    }
}
