package ElementaryFunctions;

public class Cos extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.cos(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return (new Sin()).mul(DifferentiableFunction.constant(-1));
    }

    @Override
    public String toString() {
        return "cos(x)";
    }
}
