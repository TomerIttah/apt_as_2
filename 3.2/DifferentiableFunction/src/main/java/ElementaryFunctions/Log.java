package ElementaryFunctions;

public class Log extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.log(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return new Recip();
    }

    @Override
    public String toString() {
        return "ln(x)";
    }
}
