package ElementaryFunctions;

public class Sin extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.sin(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return new Cos();
    }

    @Override
    public String toString() {
        return "sin(x)";
    }
}
