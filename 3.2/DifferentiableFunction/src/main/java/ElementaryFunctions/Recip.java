package ElementaryFunctions;

public class Recip extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return 1 / x;
    }

    @Override
    public DifferentiableFunction diff() {
        return DifferentiableFunction.constant(-1).mul((new Recip()).compose(DifferentiableFunction.xToNum(2)));
    }

    @Override
    public String toString() {
        return "(1 / x)";
    }
}
