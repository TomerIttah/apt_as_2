package ElementaryFunctions;

public class Arcsin extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.asin(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return (new Recip())
                .compose(DifferentiableFunction.constant(1)
                        .sub(DifferentiableFunction.xToNum(2))
                        .pow(DifferentiableFunction.constant(0.5)));
    }

    @Override
    public String toString() {
        return "arcsin(x)";
    }
}
