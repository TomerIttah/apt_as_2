package ElementaryFunctions;

public class Exp extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.exp(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return new Exp();
    }

    @Override
    public String toString() {
        return "e^x";
    }
}
