package ElementaryFunctions;

public class Ident extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return x;
    }

    @Override
    public DifferentiableFunction diff() {
        return DifferentiableFunction.constant(1);
    }

    @Override
    public String toString(){
        return "x";
    }
}
