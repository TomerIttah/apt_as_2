package ElementaryFunctions;

public class Arccos extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.acos(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return DifferentiableFunction.constant(-1).mul(
                (new Recip())
                .compose(DifferentiableFunction.constant(1)
                        .sub(DifferentiableFunction.xToNum(2))
                        .pow(DifferentiableFunction.constant(0.5))));
    }

    @Override
    public String toString() {
        return "arccos(x)";
    }
}
