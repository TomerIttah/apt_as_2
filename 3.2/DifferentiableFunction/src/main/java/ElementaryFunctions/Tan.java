package ElementaryFunctions;

public class Tan extends DifferentiableFunction {
    @Override
    public double valueAt(double x) {
        return Math.tan(x);
    }

    @Override
    public DifferentiableFunction diff() {
        return (new Recip()).compose(DifferentiableFunction.xToNum(2).compose(new Cos()));
    }

    @Override
    public String toString() {
        return "tan(x)";
    }
}
