import ElementaryFunctions.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestSexpr {
    @Test
    public void testElementaryFunctionDiff() {
        DifferentiableFunction arccos = new Arccos();
        DifferentiableFunction arcsin = new Arcsin();
        DifferentiableFunction arctan = new Arctan();
        DifferentiableFunction cos = new Cos();
        DifferentiableFunction exp = new Exp();
        DifferentiableFunction indent = new Ident();
        DifferentiableFunction log = new Log();
        DifferentiableFunction recip = new Recip();
        DifferentiableFunction sin = new Sin();
        DifferentiableFunction tan = new Tan();

        assertEquals("((-1.0) * ((1 / (((1.0 - x^2.0) ^ (0.5))))))", arccos.diff().toString());
        assertEquals("(1 / (((1.0 - x^2.0) ^ (0.5))))", arcsin.diff().toString());
        assertEquals("(1 / (x^2.0 + 1.0))", arctan.diff().toString());
        assertEquals("((sin(x)) * (-1.0))", cos.diff().toString());
        assertEquals("e^x", exp.diff().toString());
        assertEquals("1.0", indent.diff().toString());
        assertEquals("(1 / x)", log.diff().toString());
        assertEquals("((-1.0) * ((1 / (x^2.0))))", recip.diff().toString());
        assertEquals("cos(x)", sin.diff().toString());
        assertEquals("(1 / ((cos(x))^2.0))", tan.diff().toString());
    }

    @Test
    public void testPolynomDiff() {
        // 3x^2 + 1
        DifferentiableFunction polynom = (DifferentiableFunction.constant(3)
                .mul(DifferentiableFunction.xToNum(2))
                .add(DifferentiableFunction.constant(1)));

        assertEquals("((3.0) * (((2.0) * (x)))) + ((0.0) * (x^2.0)) + 0.0", polynom.diff().toString());
    }

    @Test
    public void testSqrtDiff() {
        // x^0.5
        DifferentiableFunction sqrt = DifferentiableFunction.xToNum(0.5);

        assertEquals("((0.5) * (x^-0.5))", sqrt.diff().toString());
    }
}
