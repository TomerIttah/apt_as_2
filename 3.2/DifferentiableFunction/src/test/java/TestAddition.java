import ElementaryFunctions.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestAddition {
    @Test
    public void testSinPolynomAdd() {
        // (2x^2 + 2) + 3sin(x)
        DifferentiableFunction fx = (DifferentiableFunction.constant(2).mul(DifferentiableFunction.xToNum(2))
                .add(DifferentiableFunction.constant(2)))
                .add(DifferentiableFunction.constant(3).mul(new Sin()));

        // Verify on some point
        assertEquals(2*Math.pow(Math.PI/2, 2) + 5, fx.valueAt(Math.PI/2), 0.1);
        assertEquals(4*(Math.PI/2), fx.diff().valueAt(Math.PI/2), 0.1);
    }

    @Test
    public void testExpTanComposeAdd() {
        // e^(x^2) + tan(2x)
        DifferentiableFunction fx = (new Exp()).compose(DifferentiableFunction.xToNum(2))
                .add((new Tan()).compose(DifferentiableFunction.constant(2).mul(new Ident())));

        // Verify on some point
        assertEquals(Math.exp(Math.pow(Math.PI, 2)) + Math.tan(Math.PI), fx.valueAt(Math.PI), 0.1);
        assertEquals(2*Math.PI*Math.exp(Math.pow(Math.PI, 2))+2, fx.diff().valueAt(Math.PI), 0.1);
    }
}
