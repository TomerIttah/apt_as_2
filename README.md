# Advanced Programming Methods - Assignment II

##### Tomer Ittah 308193150

### Prerequisites:
- Git installed on the computer with working CLI.

- JAVA (preferably version >= 8).

- Apache Maven.


### Instructions:
- Clone this repo to a local directory

  `$ git clone https://TomerIttah@bitbucket.org/TomerIttah/apt_as_2.git`

- Further instructions are in the Assignment documentation PDF.


### Statement:
- I assert that the work I submitted is 100% my own. I have not received any
part from any other student in the class, nor have I give parts of it for use to others.
Nor have I used code from other sources: Courses taught previously at this university,
courses taught at other universities, various bits of code found on the Internet, etc.
I realize that should my code be found to contain code from other sources, that a
formal case shall be opened against us with va’adat mishma’at, in pursuit of disciplinary
action.
